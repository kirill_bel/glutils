#ifndef COMMON_H
#define COMMON_H

#include "stdpch.h"
#include "log.h"

#define CONCAT_INDIRECT(FIRST, SECOND) __CONCAT(FIRST, SECOND)
#define GRF_PIMPL_PRIVATE_DECL(type) __CONCAT(type, Private)
#define GRF_PIMPL_PRIVATE(type) __CONCAT(type::type, Private)
//CONCAT( CONCAT( , type), Private)

#define GRF_PIMPL(type) \
    private: \
        struct GRF_PIMPL_PRIVATE_DECL(type); \
        inline GRF_PIMPL_PRIVATE(type)* dptr(); \
        inline const GRF_PIMPL_PRIVATE(type)* const dptr() const; \
        std::shared_ptr<const GRF_PIMPL_PRIVATE(type)> m_ptr; \
    public: \
        type clone() const; \
        void detach(); \
        bool operator==(const type& rhs) const; \
    private:

#define GRF_PIMPL_IMPL(type) \
    void type::detach() { \
        if(m_ptr.use_count() != 1) { \
            m_ptr.reset(new GRF_PIMPL_PRIVATE(type)(*m_ptr.get())); \
        } \
    } \
    type type::clone() const { \
        type buffer(*this); \
        buffer.detach(); \
        return buffer; \
    } \
    GRF_PIMPL_PRIVATE(type) *type::dptr() { \
        detach(); \
        return const_cast<GRF_PIMPL_PRIVATE(type)*>(m_ptr.get()); \
    } \
    const GRF_PIMPL_PRIVATE(type)* const type::dptr() const { \
        return m_ptr.get(); \
    } \
    bool type::operator==(const type& rhs) const { \
        return this->m_ptr == rhs.m_ptr;\
    }


namespace grf {

bool checkOpenGLError(bool quiet = true);

int sizeOfType(GLenum val);

template<typename T>
using ptr = std::shared_ptr<T>;

class Object : public std::enable_shared_from_this<Object>
{
public:
    virtual ~Object() {}
};

}

template<typename T>
inline grf::log::Logger operator<<(grf::log::Logger dbg, const glm::tvec2<T> &v)
{
    dbg.nospace() << "[" << v.x << ", " << v.y << "]";
    return dbg.space();
}

template<typename T>
inline grf::log::Logger operator<<(grf::log::Logger dbg, const glm::tvec3<T> &v)
{
    dbg.nospace() << "[" << v.x << ", " << v.y << ", " << v.z << "]";
    return dbg.space();
}

template<typename T>
inline grf::log::Logger operator<<(grf::log::Logger dbg, const glm::tvec4<T> &v)
{
    dbg.nospace() << "[" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << "]";
    return dbg.space();
}

template<typename T>
inline grf::log::Logger operator<<(grf::log::Logger dbg, const glm::tquat<T> &v)
{
    dbg.nospace() << "[" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << "]";
    return dbg.space();
}

#endif // COMMON_H
