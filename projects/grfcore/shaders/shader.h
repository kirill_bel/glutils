#ifndef SHADER_H
#define SHADER_H

#include "grfcore/common.h"
#include <string>

struct ShaderPrivate;

namespace grf {

class Shader
{
    GRF_PIMPL(Shader)
public:
    enum Status {kCompiled, kDirty};
public:
    Shader();
    explicit Shader(const std::string &source, GLenum type);
    Shader(const Shader& other);

    void update(bool force = false);

    Status status() const;
    void setStatus(Status status);

    std::string source() const;
    void setSource(const std::string &source);

    GLenum type() const;
    void setType(GLenum type);

    GLuint id() const;

    bool isValid() const;

private:

};

} //grf

#endif // SHADER_H
