#include "shader.h"

using namespace grf;

struct Shader::ShaderPrivate
{
    ShaderPrivate(const std::string &source, GLenum type) :
        source(source),
        type(type),
        status(Shader::kDirty),
        id(0)
    {}

    ShaderPrivate(const ShaderPrivate& other) :
        ShaderPrivate(other.source, other.type)
    {

    }

    ShaderPrivate() :
        ShaderPrivate("",0)
    {}

    ~ShaderPrivate()
    {
        if(id) glDeleteShader(id);
    }

    std::string source;
    GLenum type;
    GLuint id;
    Shader::Status status;
};

GRF_PIMPL_IMPL(Shader)

Shader::Shader() :
    m_ptr(new ShaderPrivate())
{

}

Shader::Shader(const std::string &source, GLenum type) :
    m_ptr(new ShaderPrivate(source, type))
{

}

Shader::Shader(const Shader &other) :
    m_ptr(other.m_ptr)
{

}

void Shader::update(bool force)
{
    if((status() == kCompiled) && !force)
        return;

    if(!dptr()->id)
        dptr()->id = glCreateShader(dptr()->type);

    dptr()->status = kDirty;

    if(dptr()->source.empty())
        return;

    const GLchar* source = static_cast<const GLchar*>(dptr()->source.c_str());
    GLint len = dptr()->source.size();
    glShaderSource(id(), 1, &source, &len);
    if(!checkOpenGLError())
        return;

    glCompileShader(id());
    if(!checkOpenGLError())
        return;


    int compiled, length;
    glGetShaderiv(id(), GL_COMPILE_STATUS, &compiled);
    if (!compiled)
    {
        std::vector<char> log;

        glGetShaderiv(id(), GL_INFO_LOG_LENGTH, &length);
        log.resize(length + 1);
        if(length)
        {
            glGetShaderInfoLog(id(), length, &length, &log[0]);
            glDeleteShader(id());
            dptr()->id = 0;
        }

        grfError() << "Shader compilation error:" << &log[0];        
    }
    else
        dptr()->status = kCompiled;
}

Shader::Status Shader::status() const
{
    return dptr()->status;
}

void Shader::setStatus(Shader::Status status)
{
    dptr()->status = status;
}

std::string Shader::source() const
{
    return dptr()->source;
}

void Shader::setSource(const std::string &source)
{
    dptr()->source = source;
    setStatus(kDirty);
}

GLenum Shader::type() const
{
    return dptr()->type;
}

void Shader::setType(GLenum type)
{
    dptr()->type = type;
    setStatus(kDirty);
}

GLuint Shader::id() const
{
    return dptr()->id;
}

bool Shader::isValid() const
{
    return id() != 0;
}
