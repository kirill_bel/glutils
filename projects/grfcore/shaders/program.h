#ifndef PROGRAM_H
#define PROGRAM_H

#include "shader.h"

namespace grf {

class Program
{
    GRF_PIMPL(Program)
    void lazyInit();
public:
    enum Status { kLinked, kDirty };
    enum VariableType { kAttribute, kUniform };
    struct Variable
    {
        GLint idx = -1;
        GLint location = -1;
        GLint size = 0;
        GLenum type = 0;
        std::string name;
    };
public:
    Program();
    Program(const Program& other);

    bool link();
    void bind();
    void unbind();

    GLuint id() const;

    Status status() const;
    void setStatus(Status status);

    Shader fragmentShader() const;
    Shader vertexShader() const;
    Shader geometryShader() const;

    void setFragmentShader(const Shader& shader);
    void setVertexShader(const Shader& shader);
    void setGeometryShader(const Shader& shader);

    GLint attributeIdx(const char* name) const;
    GLint attributeLocation(const char* name) const;
    GLint uniformIdx(const char* name) const;

    size_t attributeCount() const;
    size_t uniformCount() const;

    const Variable& attributeAt(GLint idx) const;
    const Variable& uniformAt(GLint idx) const;

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, T value);

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, const glm::tvec1<T>& value);

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, const glm::tvec2<T>& value);

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, const glm::tvec3<T>& value);

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, const glm::tvec4<T>& value);

    template<class T,
             class = typename std::enable_if<std::is_arithmetic<T>::value>::type >
    bool setUniform(GLint idx, const glm::tmat4x4<T>& value);

    template<class T>
    bool setUniform(const char* name, const T& value)
    {
        return setUniform(uniformIdx(name), value);
    }

protected:
    bool updateAttributesAndUniforms();
};

} // grf

#include "program.inl"

#endif // PROGRAMM_H
