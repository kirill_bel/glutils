#ifndef SHADERS_INL_H
#define SHADERS_INL_H

namespace grf {

    template<typename T>
    inline bool setUniform1(GLint location, GLenum type, T value)
    {
        switch(type)
        {
        case GL_INT:
            glUniform1i(location, (GLuint) value);
            break;
        case GL_BOOL:
        case GL_UNSIGNED_INT:
            glUniform1ui(location, (GLuint) value);
            break;
        case GL_FLOAT:
            glUniform1f(location, (GLfloat) value);
            break;
        case GL_DOUBLE:
            glUniform1d(location, (GLdouble) value);
            break;
        default:
            assert(false && "Incorrect uniform type!");
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool setUniformVec1(GLint location, GLenum type, glm::tvec1<T> value)
    {
        return setUniform1<T>(location, type, value[0]);
    }

    template<typename T>
    inline bool setUniformVec2(GLint location, GLenum type, const glm::tvec2<T>& value)
    {
        switch(type)
        {
        case GL_INT_VEC2:
            glUniform2iv(location, 1, glm::value_ptr((glm::tvec2<GLint>) value));
            break;
        case GL_BOOL_VEC2:
        case GL_UNSIGNED_INT_VEC2:
            glUniform2uiv(location, 1, glm::value_ptr((glm::tvec2<GLuint>) value));
            break;
        case GL_FLOAT_VEC2:
            glUniform2fv(location, 1, glm::value_ptr((glm::tvec2<GLfloat>) value));
            break;
        case GL_DOUBLE_VEC2:
            glUniform2dv(location, 1, glm::value_ptr((glm::tvec2<GLdouble>) value));
            break;
        default:
            assert(false && "Incorrect uniform type!");
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool setUniformVec3(GLint location, GLenum type, const glm::tvec3<T>& value)
    {
        switch(type)
        {
        case GL_INT_VEC3:
            glUniform3iv(location, 1, glm::value_ptr((glm::tvec3<GLint>) value));
            break;
        case GL_BOOL_VEC3:
        case GL_UNSIGNED_INT_VEC3:
            glUniform3uiv(location, 1, glm::value_ptr((glm::tvec3<GLuint>) value));
            break;
        case GL_FLOAT_VEC3:
            glUniform3fv(location, 1, glm::value_ptr((glm::tvec3<GLfloat>) value));
            break;
        case GL_DOUBLE_VEC3:
            glUniform3dv(location, 1, glm::value_ptr((glm::tvec3<GLdouble>) value));
            break;
        default:
            assert(false && "Incorrect uniform type!");
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool setUniformVec4(GLint location, GLenum type, const glm::tvec4<T>& value)
    {
        switch(type)
        {
        case GL_INT_VEC4:
            glUniform4iv(location, 1, glm::value_ptr((glm::tvec4<GLint>) value));
            break;
        case GL_BOOL_VEC4:
        case GL_UNSIGNED_INT_VEC4:
            glUniform4uiv(location, 1, glm::value_ptr((glm::tvec4<GLuint>) value));
            break;
        case GL_FLOAT_VEC4:
            glUniform4fv(location, 1, glm::value_ptr((glm::tvec4<GLfloat>) value));
            break;
        case GL_DOUBLE_VEC4:
            glUniform4dv(location, 1, glm::value_ptr((glm::tvec4<GLdouble>) value));
            break;
        default:
            assert(false && "Incorrect uniform type!");
            return false;
        }
        return true;
    }

    template<typename T>
    inline bool setUniformMat(GLint location, GLenum type, bool transpose, const glm::tmat4x4<T>& value)
    {
        switch(type)
        {
        case GL_FLOAT_MAT4:
            glUniformMatrix4fv(location, 1, transpose, glm::value_ptr((glm::tmat4x4<GLfloat>) value));
            break;
        case GL_DOUBLE_MAT4:
            glUniformMatrix4dv(location, 1, transpose, glm::value_ptr((glm::tmat4x4<GLdouble>) value));
            break;
        default:
            assert(false && "Incorrect uniform type!");
            return false;
        }
        return true;
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, T value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniform1<float>(uniformAt(idx).idx, uniformAt(idx).type, value);
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, const glm::tvec1<T>& value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniformVec1<float>(uniformAt(idx).idx, uniformAt(idx).type, value);
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, const glm::tvec2<T>& value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniformVec2<float>(uniformAt(idx).idx, uniformAt(idx).type, value);
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, const glm::tvec3<T>& value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniformVec3<float>(uniformAt(idx).idx, uniformAt(idx).type, value);
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, const glm::tvec4<T>& value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniformVec4<float>(uniformAt(idx).idx, uniformAt(idx).type, value);
    }

    template<class T,
             class>
    bool Program::setUniform(GLint idx, const glm::tmat4x4<T>& value)
    {
        if((idx < 0) || (idx >= uniformCount()))
        {
            assert(false && "Invalid uniform index!");
            return false;
        }
        return setUniformMat<float>(uniformAt(idx).idx, uniformAt(idx).type, false, value);
    }


} // grf

#endif // SHADERS__H
