#include "program.h"

using namespace grf;

struct Program::ProgramPrivate
{
    ProgramPrivate() :
        id(0),
        status(Program::kDirty)
    {

    }

    ProgramPrivate(const ProgramPrivate& other) :
        fragmentShader(other.fragmentShader),
        vertexShader(other.vertexShader),
        geometryShader(other.geometryShader),
        id(0),
        status(Program::kDirty)
    {

    }

    Shader fragmentShader;
    Shader vertexShader;
    Shader geometryShader;
    GLuint id;
    Program::Status status;

    std::map<std::string, std::pair<Program::VariableType, GLuint>> variableToIndex;
    std::vector<Program::Variable> attributes;
    std::vector<Program::Variable> uniforms;
};

GRF_PIMPL_IMPL(Program)

Program::Program() :
    m_ptr(new ProgramPrivate())
{

}

Program::Program(const Program &other) :
    m_ptr(other.m_ptr)
{

}

void Program::lazyInit()
{
    if(id() == 0)
        dptr()->id = glCreateProgram();
}


bool Program::link()
{
    lazyInit();

    setStatus(kDirty);

    if(dptr()->fragmentShader.isValid())
        dptr()->fragmentShader.update();
    if(dptr()->vertexShader.isValid())
        dptr()->vertexShader.update();
    if(dptr()->geometryShader.isValid())
        dptr()->geometryShader.update();

    glLinkProgram(id());
    if(!checkOpenGLError())
        return false;

    GLint success = 0;
    glGetProgramiv(id(), GL_LINK_STATUS, &success);
    if(!success) // Report error
    {
        GLint length = 0;
        std::vector<char> log;
        glGetProgramiv(id(), GL_INFO_LOG_LENGTH, &length);
        log.resize(length + 1);
        if (length) {
            glGetProgramInfoLog(id(), length, &length, &log[0]);
            grfError() << "Link failed:" << &log[0];
        }

        return false;
    }


    if(!updateAttributesAndUniforms())
    {
        grfError() << "Failed to updateAttributesAndUniforms";
        return false;
    }

    setStatus(kLinked);
    return true;
}

void Program::bind()
{
    if(status() == kLinked)
        glUseProgram(id());
    else
        grfError() << "Trying to bind not linked programm!";
}

void Program::unbind()
{
    glUseProgram(0);
}

GLuint Program::id() const
{
    return dptr()->id;
}

Program::Status Program::status() const
{
    return dptr()->status;
}

void Program::setStatus(Program::Status status)
{
    dptr()->status = status;
}

Shader Program::fragmentShader() const
{
    return dptr()->fragmentShader;
}

Shader Program::vertexShader() const
{
    return dptr()->vertexShader;
}

Shader Program::geometryShader() const
{
    return dptr()->geometryShader;
}

void Program::setFragmentShader(const Shader &shader)
{
    lazyInit();

    if(dptr()->fragmentShader == shader)
        return;

    if(shader.type() != GL_FRAGMENT_SHADER)
    {
        assert(false && "Incorrect shader type!");
        grfError() << "Incorrect shader type!";
        return;
    }

    if(dptr()->fragmentShader.isValid())
    {
        glDetachShader(id(), dptr()->fragmentShader.id());
    }

    dptr()->fragmentShader = shader;

    if(dptr()->fragmentShader.isValid())
    {
        glAttachShader(id(), dptr()->fragmentShader.id());
    }

    setStatus(kDirty);
}

void Program::setVertexShader(const Shader &shader)
{
    lazyInit();

    if(dptr()->vertexShader == shader)
        return;

    if(shader.type() != GL_VERTEX_SHADER)
    {
        assert(false && "Incorrect shader type!");
        return;
    }

    if(dptr()->vertexShader.isValid())
    {
        glDetachShader(id(), dptr()->vertexShader.id());
    }

    dptr()->vertexShader = shader;

    if(dptr()->vertexShader.isValid())
    {
        glAttachShader(id(), dptr()->vertexShader.id());
    }

    setStatus(kDirty);
}

void Program::setGeometryShader(const Shader &shader)
{
    lazyInit();

    if(dptr()->geometryShader == shader)
        return;

    if(shader.type() != GL_GEOMETRY_SHADER)
    {
        assert(false && "Incorrect shader type!");
        return;
    }

    if(dptr()->geometryShader.isValid())
    {
        glDetachShader(id(), dptr()->geometryShader.id());
    }

    dptr()->geometryShader = shader;

    if(dptr()->geometryShader.isValid())
    {
        glAttachShader(id(), dptr()->geometryShader.id());
    }

    setStatus(kDirty);
}

GLint Program::attributeIdx(const char *name) const
{
   auto it = dptr()->variableToIndex.find(std::string(name));
   if((it == dptr()->variableToIndex.end()) || (it->second.first != kAttribute))
       return -1;
   return it->second.second;
}

GLint Program::attributeLocation(const char *name) const
{
    GLint idx = attributeIdx(name);
    if(idx == -1)
        return -1;
    return dptr()->attributes.at(idx).location;
}

GLint Program::uniformIdx(const char *name) const
{
    auto it = dptr()->variableToIndex.find(std::string(name));
    if((it == dptr()->variableToIndex.end()) || (it->second.first != kUniform))
        return -1;
    return it->second.second;
}

size_t Program::attributeCount() const
{
    return dptr()->attributes.size();
}

size_t Program::uniformCount() const
{
    return dptr()->uniforms.size();
}

const Program::Variable &Program::attributeAt(GLint idx) const
{
    assert((idx >= 0 && idx < dptr()->attributes.size()) && "Invalid attribute index!");
    return dptr()->attributes[idx];
}

const Program::Variable &Program::uniformAt(GLint idx) const
{
    assert((idx >= 0 && idx < dptr()->uniforms.size()) && "Invalid iniform index!");
    return dptr()->uniforms[idx];
}

bool Program::updateAttributesAndUniforms()
{
    lazyInit();

    if(id() == 0)
        return false;

    dptr()->variableToIndex.clear();
    dptr()->attributes.clear();
    dptr()->uniforms.clear();

    GLsizei availableNameLength = 0;

    GLsizei lengthQuery = 0;
    glGetProgramiv(id(), GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &lengthQuery);
    availableNameLength = lengthQuery;

    glGetProgramiv(id(), GL_ACTIVE_UNIFORM_MAX_LENGTH, &lengthQuery);
    availableNameLength = std::max<GLsizei>(availableNameLength, lengthQuery) + 1;

    if(!checkOpenGLError())
        return false;

    std::string name_buffer(availableNameLength, '\0');

    GLint count = 0;

    // Dump attribtes
    glGetProgramiv(id(), GL_ACTIVE_ATTRIBUTES, &count);
    dptr()->attributes.resize(count);

    for (GLint i = 0; i < count; i++)
    {
        Variable var;
        var.idx = i;
        glGetActiveAttrib(id(), (GLuint)i, availableNameLength - 1, &lengthQuery, &var.size, &var.type, &name_buffer[0]);
        if(!checkOpenGLError())
            return false;

        var.name = std::string(&name_buffer[0], lengthQuery);

        var.location = glGetAttribLocation(id(), var.name.c_str());



        grfDebug().nospace() << "Shader attribute[" << i << "]: " << var.name;

        dptr()->variableToIndex[var.name] = std::make_pair(kAttribute, i);
        dptr()->attributes[i] = var;
    }

    // Dump uniforms
    glGetProgramiv(id(), GL_ACTIVE_UNIFORMS, &count);
    dptr()->uniforms.resize(count);

    for (GLint i = 0; i < count; i++)
    {
        Variable var;
        var.idx = i;
        glGetActiveUniform(id(), (GLuint)i, availableNameLength - 1, &lengthQuery, &var.size, &var.type, &name_buffer[0]);
        if(!checkOpenGLError())
            return false;

        var.name = std::string(&name_buffer[0], lengthQuery);

        grfDebug().nospace() << "Shader uniform[" << i << "]: " << var.name;

        dptr()->variableToIndex[var.name] = std::make_pair(kUniform, i);
        dptr()->uniforms[i] = var;
    }

    return true;
}
