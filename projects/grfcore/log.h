#ifndef LOGGER_H
#define LOGGER_H

#include "stdpch.h"

namespace grf {
namespace log {

enum Level
{
    kError = 0,
    kWarning,
    kInfo,
    kDebug
};

struct Context
{
    using LogCallback = std::function<void(grf::log::Level, const grf::log::Context&, const std::string&)>;
    Context() {}
    Context(const char *file_, int line_, const char *function_) :
        line(line_), file(file_), function(function_) {}

    static void SetLogCallback(LogCallback callback);
    static void PushLogMessage(grf::log::Level level, const grf::log::Context& context, const std::string& str);

private:
    static LogCallback logCallback;
    static std::mutex logMutex;
public:
    const char *file = "";
    int line = 0;
    const char *function = "";
};

class Logger {
public:
    Logger(const Context& context, Level logLevel = kError);
    Logger(const Logger& other);
    ~Logger();

    template <typename T>
    Logger& operator<<(const T& value)
    {
        dbg_out() << value;
        return maybeSpace();
    }

    Logger& nospace() {make_spaces = false; return *this;}
    Logger& space() {make_spaces = true; return *this;}
    Logger& maybeSpace() {if(make_spaces) dbg_out() << ' '; return *this;}

private:
    std::ostream& dbg_out()
    {
        if(!stream)
            return std::cerr;
        return *stream;
    }
private:
    Level log_level;
    bool make_spaces = true;
    Context log_context;
    std::shared_ptr<std::stringstream> stream;
};

class NullLogger
{
public:
    template <typename T>
    NullLogger& operator<<(T const & value) {return *this;}
    NullLogger& nospace() {return *this;}
    NullLogger& space() {return *this;}
    NullLogger& maybeSpace() {return *this;}
};

class MessageLogger
{
public:
    MessageLogger() {}
    MessageLogger(const char *file_, int line_, const char *function_) :
            context(file_, line_, function_) {}

    Logger debug() const;
    Logger info() const;
    Logger warning() const;
    Logger error() const;
    NullLogger nullOutput() const;
private:
    Context context;
};

} // log
} // grf

#if defined _WIN32 || defined __CYGWIN__
#   define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)
#else
#   define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

#define grfDebug      grf::log::MessageLogger(__FILENAME__, __LINE__, __FUNCTION__).debug
#define grfInfo       grf::log::MessageLogger(__FILENAME__, __LINE__, __FUNCTION__).info
#define grfWarning    grf::log::MessageLogger(__FILENAME__, __LINE__, __FUNCTION__).warning
#define grfError      grf::log::MessageLogger(__FILENAME__, __LINE__, __FUNCTION__).error

#define NO_DEBUG_MACRO while (false) ::ddelib::MessageLogger().nullOutput
#if defined(NO_DEBUG_OUTPUT)
#  undef OutDebug
#  define OutDebug NO_DEBUG_MACRO
#endif

#endif // LOGGER_H
