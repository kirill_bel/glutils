#ifndef STDPCH_H
#define STDPCH_H

#pragma message ( "--------------------------- GLFCORE STDPCH! -------------------------" )

#include <GL/glew.h>
#include <cassert>
#include <memory>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <functional>
#include <mutex>
#include <sstream>
#include <map>
#include <unordered_map>
#include <algorithm>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/common.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/epsilon.hpp>


#endif // STDPCH_H
