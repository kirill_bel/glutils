#ifndef GLUTILS_H
#define GLUTILS_H

#include "grfcore/common.h"

namespace grf {

class Core
{
public:
    static Core& instance();
public:
    Core();
    ~Core();

    void initialize();
    void render();
};

} // grf

#endif // GLUTILS_H
