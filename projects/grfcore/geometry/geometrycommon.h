#ifndef GEOMETRYCOMMON_H
#define GEOMETRYCOMMON_H

namespace grf {

// Buffer interface
class Buffer
{
public:
    virtual ~Buffer() = 0;


    int size() const = 0;

};

} // grf


#endif // GEOMETRYCOMMON_H
