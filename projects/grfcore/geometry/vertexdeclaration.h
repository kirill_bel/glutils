#ifndef VERTEXDECLARATION_H
#define VERTEXDECLARATION_H

#include "grfcore/common.h"

namespace grf {

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

class VertexDeclaration
{
public:
    enum Attribute {kPosition, kColor, kNormal, kTexcoord};
private:
    struct AttributeDesc
    {
        Attribute attr;
        GLuint componentCount = 0;
        GLenum variableType;
        GLsizei stride;
        GLuint offset;
        bool separate = false;
    };
public:
    VertexDeclaration();

    int addAttribute(Attribute attr, GLuint componentCount, GLenum variableType, GLsizei stride);
    bool isEmpty() const;
    void clear();
    int vertexSize() const;
    int count() const;

    void bindAttribute(int idx);
    void unbindAttribute(int idx);
    void bind();
    void unbind();

private:
    std::vector<AttributeDesc> m_attributes;
    size_t m_offset = 0;
};

} // grf

#endif // VERTEXDECLARATION_H
