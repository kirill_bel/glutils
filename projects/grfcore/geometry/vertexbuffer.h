#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "buffer.h"

namespace grf {

template<typename T>
class VertexBuffer
{
public:
    enum Status {kUpdated, kDirty};
public:
    VertexBuffer(GLenum usage);
    ~VertexBuffer();

    GLuint id() const {return m_id;}
    Status status() const {return m_status;}
    void setStatus(Status status) {m_status = status;}

    int size() const {return m_data.size();}
    int elementSize() const {return sizeof(T);}

    void bind();
    void unbind();
    void clear();
    void resize(int newSize);

    T& at(int idx);
    const T& at(int idx) const;
    void append(const T& vertex);

    void update(bool force = false);
private:
    Status m_status = kDirty;
    GLuint m_id = 0;
    GLenum m_usage = 0;
    GLuint m_vao = 0;
    std::vector<T> m_data;
    Buffer m_buffer;
};

#include "vertexbuffer.inl"

} // grf



#endif // VERTEXBUFFER_H
