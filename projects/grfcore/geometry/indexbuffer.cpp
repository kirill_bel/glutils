#include "indexbuffer.h"

#include "buffer.h"

using namespace grf;

struct IndexBuffer::IndexBufferPrivate
{
    IndexBufferPrivate(IndexBuffer::ElementSize elementSize) :
        buffer(GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER),
        status(IndexBuffer::kDirty),
        elementSize(elementSize)
    {

    }

    IndexBufferPrivate(const IndexBufferPrivate& other) :
        buffer(GL_STATIC_DRAW, GL_ELEMENT_ARRAY_BUFFER),
        status(IndexBuffer::kDirty),
        elementSize(other.elementSize),
        elements(other.elements)
    {

    }

    IndexBuffer::Status status;
    Buffer buffer;
    IndexBuffer::ElementSize elementSize;
    std::vector<uint16_t> elements;
};

GRF_PIMPL_IMPL(IndexBuffer)

IndexBuffer::IndexBuffer(IndexBuffer::ElementSize elementSize) :
    m_ptr(new IndexBufferPrivate(elementSize))
{
}

IndexBuffer::IndexBuffer(const IndexBuffer &other) :
    m_ptr(other.m_ptr)
{

}

IndexBuffer::Status IndexBuffer::status() const
{
    return dptr()->status;
}

void IndexBuffer::setStatus(IndexBuffer::Status status)
{
    dptr()->status = status;
}

int IndexBuffer::size() const
{
    return dptr()->elements.size() * sizeof(uint16_t) / elementSize();
}

int IndexBuffer::elementSize() const
{
    return dptr()->elementSize;
}

uint32_t IndexBuffer::at(int index) const
{
    if((index < 0) || (index >= size()))
    {
        assert(false && "grf::IndexBuffer: Bad index");
        return 0;
    }

    switch(elementSize())
    {
    case k16bit:
        return dptr()->elements[index];
    case k32bit:
        return reinterpret_cast<const uint32_t*>(&(dptr()->elements.front()))[index];
    default:
        assert(false && "grf::IndexBuffer: Unknown element size");
        return 0;
    }
}

void IndexBuffer::set(int index, uint32_t value)
{
    if((index < 0) || (index >= size()))
    {
        assert(false && "grf::IndexBuffer: Bad index");
        return;
    }

    switch(elementSize())
    {
    case k16bit:
        dptr()->elements[index] = value;
        break;
    case k32bit:
        reinterpret_cast<uint32_t*>(&(dptr()->elements.front()))[index] = value;
        break;
    default:
        assert(false && "grf::IndexBuffer: Unknown element size");
    }

    setStatus(kDirty);
}

void IndexBuffer::append(uint32_t value)
{
    int idx = size();
    resize(idx + 1);
    set(idx, value);
}

void IndexBuffer::resize(int size)
{
    dptr()->elements.resize(size * elementSize() / sizeof(uint16_t));
    setStatus(kDirty);
}

void IndexBuffer::clear()
{
    dptr()->elements.clear();
    setStatus(kDirty);
}

void IndexBuffer::bind()
{
    update();
    dptr()->buffer.bind();
}

void IndexBuffer::unbind()
{
    dptr()->buffer.unbind();
}

GLuint IndexBuffer::id() const
{
    return dptr()->buffer.id();
}

void IndexBuffer::update(bool force)
{
    if((dptr()->status == kUpdated) && !force)
        return;

    GLuint bufSize = size() * elementSize();
    void* data = dptr()->elements.empty() ? nullptr : &(dptr()->elements.front());
    dptr()->buffer.reset(data, bufSize);

    setStatus(kUpdated);
}
