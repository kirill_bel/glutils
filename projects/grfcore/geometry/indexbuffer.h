#ifndef INDEXBUFFER_H
#define INDEXBUFFER_H

#include "grfcore/common.h"

namespace grf {

class IndexBuffer
{
    GRF_PIMPL(IndexBuffer)
public:
    enum Status {kUpdated, kDirty};
    enum ElementSize
    {
        k16bit = 2,
        k32bit = 4
    };
public:
    explicit IndexBuffer(ElementSize elementSize = k32bit);
    IndexBuffer(const IndexBuffer& other);

    Status status() const;
    void setStatus(Status status);

    int size() const;
    int elementSize() const;

    uint32_t at(int index) const;
    void set(int index, uint32_t value);
    void append(uint32_t value);
    void resize(int size);
    void clear();

    void bind();
    void unbind();

    GLuint id() const;

    void update(bool force = false);
};

}
#endif // INDEXBUFFER_H
