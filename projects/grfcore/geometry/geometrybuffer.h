#ifndef GEOMETRYBUFFER_H
#define GEOMETRYBUFFER_H

#include "buffer.h"
#include "vertexdeclaration.h"

namespace grf {

class GeometryBuffer
{
    GRF_PIMPL(GeometryBuffer)
    uint8_t* data();
    const uint8_t* data() const;
public:
    enum Status {kUpdated, kDirty};
public:
    explicit GeometryBuffer(size_t vertexSize, GLenum usage = GL_STATIC_DRAW);
    explicit GeometryBuffer(const VertexDeclaration& decl, GLenum usage = GL_STATIC_DRAW);
    GeometryBuffer(const GeometryBuffer& other);

    Status status() const;
    void setStatus(Status status);

    int size() const;
    int elementSize() const;

    void update(bool force = false);

    void clear();
    void bind();
    void unbind();

    void resize(int newSize);

    template<typename T> inline
    T& at(int idx)
    {
        assert(sizeof(T) == elementSize() && "Incorrect element size!");
        setStatus(kDirty);
        return reinterpret_cast<T*>(data() + idx * elementSize())[0];
    }

    template<typename T> inline
    const T& at(int idx) const
    {
        assert(sizeof(T) == elementSize() && "Incorrect element size!");
        return reinterpret_cast<const T*>(data() + idx * elementSize())[0];
    }

    template<typename T>
    void append(const T& vertex)
    {
        assert(sizeof(T) == elementSize() && "Incorrect element size!");
        size_t idx = size();
        resize(idx + 1);
        at<T>(idx) = vertex;
    }
};

} //grf

#endif // GEOMETRYBUFFER_H
