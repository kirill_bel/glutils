#include "buffer.h"

using namespace grf;

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

grf::Buffer::Buffer(GLenum usage, GLenum target) :
    m_usage(usage), m_target(target)
{

}

Buffer::~Buffer()
{
    release();
}

GLuint Buffer::id() const
{
    return m_id;
}

int Buffer::size() const
{
    return m_size;
}

void Buffer::reset(void *data, size_t size)
{
    if(!m_id)
    {
        glGenBuffers(1, &m_id);
    }

    glBindBuffer(m_target, m_id);
    glBufferData(m_target, size, 0, m_usage);
    if(!checkOpenGLError(false))
        return;

    glBufferData(m_target, size, data, m_usage);
    if(!checkOpenGLError(false))
        return;

    m_size = size;
}

void Buffer::reset(size_t size)
{
    if(!m_id)
    {
        glGenBuffers(1, &m_id);
    }

    glBindBuffer(m_target, m_id);
    glBufferData(m_target, size, 0, m_usage);
    if(!checkOpenGLError(false))
        return;

    m_size = size;
}

void Buffer::release()
{
    if(m_id)
    {
        glDeleteBuffers(1, &m_id);
        m_id = 0;
    }
}

void *Buffer::map(size_t offset, size_t size, GLbitfield flags)
{
    if(!m_id)
    {
        assert(false && "zero id");
        return nullptr;
    }

    return glMapBufferRange(m_target, offset, size, flags);
}

void Buffer::unmap()
{
    glUnmapBuffer(m_target);
}

void Buffer::bind()
{
    if(!m_id)
    {
        assert(false && "zero id");
        return;
    }

    glBindBuffer(m_target, m_id);
    if(!checkOpenGLError(false))
        return;
}

void Buffer::unbind()
{
    glBindBuffer(m_target, 0);
}

GLenum Buffer::usage() const
{
    return m_usage;
}

GLenum Buffer::target() const
{
    return m_target;
}
