#include "vertexdeclaration.h"

using namespace grf;



VertexDeclaration::VertexDeclaration()
{

}

int VertexDeclaration::addAttribute(Attribute attr, GLuint componentCount, GLenum variableType, GLsizei stride)
{
    AttributeDesc decl;
    decl.attr = attr;
    decl.componentCount = componentCount;
    decl.variableType = variableType;
    decl.stride = stride;
    decl.offset = m_offset;
    decl.separate = false;
    m_attributes.push_back(decl);

    m_offset += componentCount * grf::sizeOfType(variableType);
    return m_attributes.size() - 1;
}

void VertexDeclaration::bindAttribute(int idx)
{
    if((idx < 0) || (idx >= m_attributes.size()))
    {
        assert(false && "VertexDeclaration: Invalid attribute index");
        return;
    }

    const AttributeDesc& attr = m_attributes[idx];

    int offset = attr.separate ? (attr.componentCount * grf::sizeOfType(attr.variableType)) : m_offset;

    switch(attr.attr)
    {
        case kPosition:
            glEnableClientState(GL_VERTEX_ARRAY);
            glVertexPointer(attr.componentCount, attr.variableType, offset, BUFFER_OFFSET(attr.offset));
            break;
        case kColor:
            glEnableClientState(GL_COLOR_ARRAY);
            glColorPointer(attr.componentCount, attr.variableType, offset, BUFFER_OFFSET(attr.offset));
            break;
        case kNormal:
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(attr.variableType, offset, BUFFER_OFFSET(attr.offset));
            break;
        case kTexcoord:
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);
            glTexCoordPointer(attr.componentCount, attr.variableType, offset, BUFFER_OFFSET(attr.offset));
            break;
    }
}

void VertexDeclaration::unbindAttribute(int idx)
{
    if((idx < 0) || (idx >= m_attributes.size()))
    {
        assert(false && "VertexDeclaration: Invalid attribute index");
        return;
    }

    const AttributeDesc& attr = m_attributes[idx];

    switch(attr.attr)
    {
    case kPosition:
        glDisableClientState(GL_VERTEX_ARRAY);
        break;
    case kColor:
        glDisableClientState(GL_COLOR_ARRAY);
        break;
    case kNormal:
        glDisableClientState(GL_NORMAL_ARRAY);
        break;
    case kTexcoord:
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        break;
    }
}

void VertexDeclaration::bind()
{
    for(int i = 0; i < m_attributes.size(); i++)
        bindAttribute(i);
}

void VertexDeclaration::unbind()
{
    for(int i = 0; i < m_attributes.size(); i++)
        unbindAttribute(i);
}

bool VertexDeclaration::isEmpty() const
{
    return m_attributes.empty();
}

void VertexDeclaration::clear()
{
    unbind();
    m_attributes.clear();
    m_offset = 0;
}

int VertexDeclaration::vertexSize() const
{
    return m_offset;
}

int VertexDeclaration::count() const
{
    return m_attributes.size();
}
