#ifndef BUFFER_H
#define BUFFER_H

#include "grfcore/common.h"

namespace grf {

class Buffer
{
public:
    Buffer(GLenum usage, GLenum target);
    Buffer(const Buffer& other) = delete;
    ~Buffer();

    GLuint id() const;
    int size() const;
    void reset(void* data, size_t size);
    void reset(size_t size);
    void release();

    void* map(size_t offset, size_t size, GLbitfield flags);
    void unmap();

    void bind();
    void unbind();

    GLenum usage() const;
    GLenum target() const;

private:
    GLuint m_id = 0;
    size_t m_size = 0;
    GLenum m_usage = 0;
    GLenum m_target = 0;
};

} // grf

#endif // BUFFER_H
