#include "geometrybuffer.h"

using namespace grf;

struct GeometryBuffer::GeometryBufferPrivate
{
    GeometryBufferPrivate(const std::vector<uint8_t>& data, const VertexDeclaration &decl, GLenum usage) :
        buffer(usage, GL_ARRAY_BUFFER),
        status(GeometryBuffer::kDirty),
        decl(decl),
        vertexSize(0),
        data(data)
    {
    }

    GeometryBufferPrivate(const std::vector<uint8_t>& data, size_t vertexSize, GLenum usage) :
        buffer(usage, GL_ARRAY_BUFFER),
        status(GeometryBuffer::kDirty),
        vertexSize(vertexSize),
        data(data)
    {
    }


    GeometryBufferPrivate(size_t vertexSize, GLenum usage) :
        GeometryBufferPrivate(std::vector<uint8_t>(), vertexSize, usage)
    {
    }

    GeometryBufferPrivate(const GeometryBufferPrivate& other) :
        GeometryBufferPrivate(other.data, other.decl, other.buffer.usage())
    {

    }

    size_t getVertexSize() const
    {
        return vertexSize ? vertexSize : decl.vertexSize();
    }

    GeometryBuffer::Status status;
    VertexDeclaration decl;
    size_t vertexSize;
    std::vector<uint8_t> data;
    GLuint vao = 0;
    Buffer buffer;
};

GRF_PIMPL_IMPL(GeometryBuffer)

GeometryBuffer::GeometryBuffer(size_t vertexSize, GLenum usage) :
    m_ptr(new GeometryBufferPrivate(vertexSize, usage))
{

}

grf::GeometryBuffer::GeometryBuffer(const VertexDeclaration &decl, GLenum usage) :
     m_ptr(new GeometryBufferPrivate(std::vector<uint8_t>(), decl, usage))
{

}

grf::GeometryBuffer::GeometryBuffer(const GeometryBuffer &other) :
    m_ptr(other.m_ptr)
{

}

uint8_t *GeometryBuffer::data()
{
    return &(dptr()->data.front());
}

const uint8_t *GeometryBuffer::data() const
{
    return &(dptr()->data.front());
}

GeometryBuffer::Status GeometryBuffer::status() const
{
    return dptr()->status;
}

void GeometryBuffer::setStatus(GeometryBuffer::Status status)
{
    dptr()->status = status;
}

int GeometryBuffer::size() const
{
    return dptr()->data.size() / elementSize();
}

int GeometryBuffer::elementSize() const
{
    return dptr()->getVertexSize();
}

void GeometryBuffer::update(bool force)
{
    if((status() == kUpdated) && !force)
        return;

    if(!dptr()->vao)
        glGenVertexArrays(1, &(dptr()->vao));

    glBindVertexArray(dptr()->vao);
    if(!checkOpenGLError(false))
        return;

    void* data = dptr()->data.empty() ? nullptr : &(dptr()->data.front());
    dptr()->buffer.reset(data, dptr()->data.size());

    glBindVertexArray(0);

    setStatus(kUpdated);
}

void GeometryBuffer::clear()
{
    dptr()->data.clear();
    setStatus(kDirty);
}

void GeometryBuffer::bind()
{
    update();

    glBindVertexArray(dptr()->vao);
    if(!checkOpenGLError(false))
        return;
}

void GeometryBuffer::unbind()
{
    glBindVertexArray(0);
}

void GeometryBuffer::resize(int newSize)
{
    dptr()->data.resize(newSize * elementSize());
    setStatus(kDirty);
}
