
template<typename T>
VertexBuffer<T>::VertexBuffer(GLenum usage) :
    m_buffer(usage, GL_ARRAY_BUFFER)
{

}

template<typename T>
VertexBuffer<T>::~VertexBuffer()
{

}

template<typename T>
void VertexBuffer<T>::bind()
{
    update();

    glBindVertexArray(m_vao);
    if(!checkOpenGLError(false))
        return;
}

template<typename T>
void VertexBuffer<T>::unbind()
{
    glBindVertexArray(0);
}

template<typename T>
void VertexBuffer<T>::clear()
{
    m_data.clear();
    setStatus(kDirty);
}

template<typename T>
void VertexBuffer<T>::resize(int newSize)
{
    m_data.resize(newSize);
    setStatus(kDirty);
}

template<typename T>
T& VertexBuffer<T>::at(int idx)
{
    setStatus(kDirty);
    return m_data[idx];
}

template<typename T>
const T& VertexBuffer<T>::at(int idx) const
{
    return m_data[idx];
}

template<typename T>
void VertexBuffer<T>::append(const T& vertex)
{
    m_data.push_back(vertex);
    setStatus(kDirty);
}

template<typename T>
void VertexBuffer<T>::update(bool force)
{
    if((status() == kUpdated) && !force)
        return;

    if(!m_vao)
        glGenVertexArrays(1, &m_vao);

    glBindVertexArray(m_vao);
    if(!checkOpenGLError(false))
        return;

    void* data = m_data.empty() ? nullptr : &(m_data.front());
    m_buffer.reset(data, m_data.size() * elementSize());

    glBindVertexArray(0);

    setStatus(kUpdated);
}
