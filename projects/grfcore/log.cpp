#include "log.h"

using namespace grf::log;

Context::LogCallback Context::logCallback;
std::mutex Context::logMutex;

void grf::log::Context::SetLogCallback(grf::log::Context::LogCallback callback)
{
    Context::logCallback = callback;
}

void Context::PushLogMessage(Level level, const Context &context, const std::string &str)
{
    logMutex.lock();

    if(logCallback)
        logCallback(level, context, str);

    logMutex.unlock();
}

Logger::Logger(const Context& context, Level logLevel) :
        log_context(context), log_level(logLevel),
        stream(new std::stringstream)
{
}

Logger::Logger(const Logger& other)
{
    log_level = other.log_level;
    make_spaces = other.make_spaces;
    log_context = other.log_context;
    stream = other.stream;
}

Logger::~Logger()
{
    if(stream.use_count() <= 1)
    {
        nospace() << "\n";
        Context::PushLogMessage(log_level, log_context, stream->str());
    }
}

Logger MessageLogger::debug() const
{
    return Logger(context, kDebug);
}

Logger MessageLogger::info() const
{
    return Logger(context, kInfo);
}

Logger MessageLogger::warning() const
{
    return Logger(context, kWarning);
}

Logger MessageLogger::error() const
{
    return Logger(context, kError);
}

NullLogger MessageLogger::nullOutput() const
{
    return NullLogger();
}
