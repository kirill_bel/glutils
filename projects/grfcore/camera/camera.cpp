#include "camera.h"

using namespace grf;

Camera::Camera()
{

}

float Camera::nearPlane() const
{
    return m_nearPlane;
}

void Camera::setNearPlane(float near)
{
    m_nearPlane = near;
    m_status = kDirty;
}

float Camera::farPlane() const
{
    return m_farPlane;
}

void Camera::setFarPlane(float far)
{
    m_farPlane = far;
    m_status = kDirty;
}

float Camera::fovAngle() const
{
    return m_fovAngle;
}

void Camera::setFovAngle(float fov)
{
    m_fovAngle = fov;
    m_status = kDirty;
}

float Camera::aspect() const
{
    return m_aspect;
}

void Camera::setAspect(float aspect)
{
    m_aspect = aspect;
    m_status = kDirty;
}

glm::mat4 Camera::view() const
{
    return m_view;
}

void Camera::setView(const glm::mat4 &view)
{
    m_view = view;
    m_status = kDirty;
}

glm::mat4 Camera::projection() const
{
    return m_projection;
}

Camera::Status Camera::status() const
{
    return m_status;
}

void Camera::setStatus(Camera::Status status)
{
    m_status = status;
}

void Camera::update(bool force)
{
    if((m_status == kUpdated) && !force)
        return;

    m_projection = glm::perspective(glm::radians(35.f), 1.77777f, 1.f, 1000.f);
    m_status = kUpdated;
}


