#ifndef CAMERA_H
#define CAMERA_H

#include "grfcore/common.h"

namespace grf {

class Camera
{
public:
    enum Status {kUpdated, kDirty};
public:
    Camera();

    float nearPlane() const;
    void setNearPlane(float near);

    float farPlane() const;
    void setFarPlane(float far);

    float fovAngle() const;
    void setFovAngle(float fovAngle);

    float aspect() const;
    void setAspect(float aspect);

    glm::mat4 view() const;
    void setView(const glm::mat4 &view);

    glm::mat4 projection() const;

    Status status() const;
    void setStatus(Status status);

    void update(bool force = false);

private:
    float m_nearPlane = 1;
    float m_farPlane = 1000;
    float m_fovAngle = 1;
    float m_aspect = 1;

    glm::mat4 m_projection;
    glm::mat4 m_view;

    Status m_status = kDirty;
};

}

#endif // CAMERA_H
