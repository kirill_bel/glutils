#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "grfcore/common.h"

namespace grf {

template<typename T>
class TransformT
{
public:
    using Position = glm::tvec3<T>;
    using Scale =    glm::tvec3<T>;
    using Rotation = glm::tquat<T>;
    using Matrix = glm::tmat4x4<T>;
public:
    explicit TransformT(const Position& pos, const Rotation& rot, const Scale& scale) :
        p(pos), q(rot), s(scale) {assert(isValid() && "Transform is not valid!");}
    explicit TransformT(const Position& pos, const Rotation& rot) :
        TransformT(pos, rot, Scale(1,1,1)) {}
    explicit TransformT(const Rotation& rot) :
        TransformT(Position(0,0,0), rot, Scale(1,1,1)) {}
    explicit TransformT(const Position& pos) :
        TransformT(pos, Rotation(), Scale(1,1,1)) {}
    explicit TransformT() :
        TransformT(Position(0,0,0), Rotation(), Scale(1,1,1)) {}

    explicit TransformT(const Matrix& mat) {setMatrix(mat);}


    bool isValid() const
    {
        return !glm::any(glm::isinf(p)) &&
               !glm::any(glm::isinf(glm::tvec4<T>(q.x, q.y, q.z, q.w))) &&
               !glm::any(glm::isinf(s));
    }

    void setMatrix(const Matrix& mat)
    {
        q = glm::toQuat(mat);
        p = Position(mat[3]);
        s = Scale(1,1,1);
    }

    Matrix matrix() const
    {
        Matrix mat = glm::toMat4(q);
        mat[3] = glm::tvec4<T>(p,1);
        return glm::scale(mat, s);
    }

    void set(const Position& pos) {p = pos;}
    void set(const Position& pos, const Rotation& rot) {p = pos; q = rot;}
    void set(const Position& pos, const Rotation& rot, const Scale& scale_) {p = pos; q = rot; s = scale_;}

    Position forward() const {return glm::normalize(q * Position(0,0,1));}
    Position right() const {return glm::normalize(q * Position(1,0,0));}
    Position up() const {return glm::normalize(q * Position(0,1,0));}

    TransformT<T>& transform(const TransformT<T>& other) {Position newP = q * other.p + p; Rotation newR = q * other.q; set(newP,newR); return *this;}
    TransformT<T>& transform(const Matrix& other) {transform(TransformT(other)); return *this;}
    TransformT<T>& move(const Position& val)    {p += val; return *this;}
    TransformT<T>& scale(const Scale& val)      {s *= val; return *this;}
    TransformT<T>& rotate(const Rotation& val)  {q = q * val; return *this;}
    TransformT<T>& inverse() {setMatrix(glm::inverse(matrix()));}

    TransformT<T> transformed(const TransformT<T>& other) const {TransformT<T> tr(*this); tr.transform(other); return tr;}
    TransformT<T> transformed(const Matrix& other) const        {TransformT<T> tr(*this); tr.transform(other); return tr;}
    TransformT<T> moved(const Position& val) const              {TransformT<T> tr(*this); tr.move(val); return tr;}
    TransformT<T> rotated(const Rotation& val) const            {TransformT<T> tr(*this); tr.rotate(val); return tr;}
    TransformT<T> scaled(const Scale& val) const                {TransformT<T> tr(*this); tr.scale(val); return tr;}
    TransformT<T> inverted() const                              {TransformT<T> tr(*this); tr.inverse(); return tr;}

    TransformT<T>& operator=(const TransformT<T>& other) {q = other.q; p = other.p; s = other.s; return *this;}
    bool operator==(const TransformT<T>& other) const {return (q == other.q) && (p = other.p) && (s = other.s);}
    bool operator!=(const TransformT<T>& other) const {return !((*this) == other);}

    bool equals(const TransformT<T>& other, T epsilon = FLT_EPSILON) const {return glm::all(glm::epsilonEqual(p, other.p, epsilon)) &&
                                                                                   glm::all(glm::epsilonEqual(s, other.s, epsilon)) &&
                                                                                   glm::all(glm::epsilonEqual(q, other.q, epsilon));}

    TransformT<T> operator*(const TransformT<T>& other) const {return this->transformed(other);}
    TransformT<T>& operator*=(const TransformT<T>& other) {return this->transform(other);}

public:
    Position p;
    Scale s;
    Rotation q;
};

typedef TransformT<float> TransformF;
typedef TransformT<double> TransformD;
typedef TransformF Transform;

} // grf

#endif // TRANSFORM_H
