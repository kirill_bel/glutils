#ifndef RECT_H
#define RECT_H

#include "grfcore/common.h"

namespace grf {

template<typename T>
class trect
{
public:
    trect(void) : top(0),bottom(0),left(0),right(0)  {}

    trect(T l, T t, T width, T height) :
        left(l), top(t),
        right(l+width),
        bottom(t+height)
    {}

    trect(const glm::tvec2<T>& topLeft, const glm::tvec2<T>& bottomRight) :
        left(topLeft.x), top(topLeft.y),
        right(bottomRight.x),
        bottom(bottomRight.y)
    {}

    void set(T l, T t, T r, T b) {top = t; bottom = b; left = l; right = r;}

    inline glm::tvec2<T> topLeft() const       {return glm::tvec2<T>(left, top);}
    inline glm::tvec2<T> topRight() const      {return glm::tvec2<T>(right, top);}
    inline glm::tvec2<T> bottomLeft() const    {return glm::tvec2<T>(left, bottom);}
    inline glm::tvec2<T> bottomRight() const   {return glm::tvec2<T>(right, bottom);}

    inline T width() const {return glm::abs(right - left);}
    inline T height() const {return glm::abs(bottom - top);}
    inline glm::tvec2<T> size() const {return glm::tvec2<T>(width(), height());}
    inline glm::tvec2<T> center() const {return glm::tvec2<T>(left + width() / (T)2, top + height() / (T)2);}

    template<typename U>
    inline void move(const glm::tvec2<U>& vec) {left+=vec.x; right+=vec.x; top+=vec.y; bottom+=vec.y;}

    template<typename U>
    bool contains(const glm::tvec2<U>& vec, bool proper = false) const
    {
        return proper ? ((vec.x >  glm::min<U>(left, right)) &&  (vec.x < glm::max<U>(left, right)) &&
                         (vec.y >  glm::min<U>(top, bottom)) &&  (vec.y < glm::max<U>(top, bottom))) :
                        ((vec.x >= glm::min<U>(left, right)) && (vec.x <= glm::max<U>(left, right)) &&
                         (vec.y >= glm::min<U>(top, bottom)) && (vec.y <= glm::max<U>(top, bottom)));
    }

    template<typename U>
    bool contains(const trect<U>& rect, bool proper = false) const
    {
        return contains(rect.topLeft(), proper) && contains(rect.topRight(), proper)  &&
               contains(rect.bottomLeft(), proper) && contains(rect.bottomRight(), proper);
    }

    template<typename U>
    bool intersects(const trect<U>& rect, bool proper = false) const
    {
        return proper ? ((left < rect.right) && (right > rect.left) &&
                         (top < rect.bottom) && (bottom > rect.top)) :
                        ((left <= rect.right) && (right >= rect.left) &&
                         (top <= rect.bottom) && (bottom >= rect.top));
    }

    bool compare(const trect<T> &other, T epsilon = 0) const {
        return (glm::distance(top, other.top) < epsilon) &&
                (glm::distance(bottom, other.bottom) < epsilon) &&
                (glm::distance(left, other.left) < epsilon) &&
                (glm::distance(right, other.right) < epsilon);
    }

    template<typename U>
    inline void operator = (const trect<U> &other) {set(other.left, other.top, other.right, other.bottom);}
    inline bool operator == (const trect<T> &other)  { return compare(other);}
    inline bool operator != (const trect<T> &other) { return !(*this == other); }

    T top;
    T bottom;
    T left;
    T right;
};


typedef trect<int> recti;
typedef trect<float> rectf;
typedef trect<double> rectd;

typedef recti rect;

} // grf

template<typename T>
inline grf::log::Logger operator<<(grf::log::Logger dbg, const grf::trect<T> &v)
{
    dbg.nospace() << "[" << v.left << ", " << v.top << ", w=" << v.width() << ", h=" << v.height() << "]";
    return dbg.space();
}

#endif // RECT_H
