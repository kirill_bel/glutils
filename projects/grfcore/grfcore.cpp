#include "grfcore.h"

#include <GL/glew.h>
#include <sstream>

void MessageCallback( GLenum source,
                      GLenum type,
                      GLuint id,
                      GLenum severity,
                      GLsizei length,
                      const GLchar* message,
                      const void* userParam )
{
    const char* label = "UNKNOWN";
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
        label = "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        label = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        label = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        label = "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        label = "PERFORMANCE";
        break;
    case GL_DEBUG_TYPE_OTHER:
        label = "OTHER";
        break;
    }

    switch (severity){
    case GL_DEBUG_SEVERITY_LOW:
    case GL_DEBUG_SEVERITY_MEDIUM:
    case GL_DEBUG_SEVERITY_HIGH:
        grfWarning().nospace() << "[OpenGL|" << label << "|" << id << "]: " << message;
        break;
    default:
//        grfDebug().nospace() << "[OpenGL] " << message;
        break;
    }
}

using namespace grf;

Core &Core::instance()
{
    static Core utils;
    return utils;
}

Core::Core()
{

}

Core::~Core()
{

}

void Core::initialize()
{
    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK)
    {
        assert(false);
        return;
    }

    glEnable( GL_DEBUG_OUTPUT );
    if(glDebugMessageCallback){
        grfInfo() << "Register OpenGL debug callback";
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(MessageCallback, nullptr);
        GLuint unusedIds = 0;
        glDebugMessageControl(GL_DONT_CARE,
            GL_DONT_CARE,
            GL_DONT_CARE,
            0,
            &unusedIds,
            true);
    }
    else
        grfError() << "glDebugMessageCallback not available";
}

void Core::render()
{
    glDrawArrays(GL_TRIANGLES, 0, 1);
}

