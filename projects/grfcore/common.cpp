#include "common.h"

using namespace grf;

int grf::sizeOfType(GLenum val)
{
    switch(val)
    {
    case GL_BYTE           : return 1;
    case GL_UNSIGNED_BYTE  : return 1;
    case GL_SHORT          : return 2;
    case GL_UNSIGNED_SHORT : return 2;
    case GL_INT            : return 4;
    case GL_UNSIGNED_INT   : return 4;
    case GL_FLOAT          : return 4;
    case GL_2_BYTES        : return 2;
    case GL_3_BYTES        : return 3;
    case GL_4_BYTES        : return 4;
    case GL_DOUBLE         : return 8;
    }
    return 0;
}

bool grf::checkOpenGLError(bool quiet)
{
    GLenum err;
    bool hasError = false;
    while((err = glGetError()) != GL_NO_ERROR)
    {
        grfError() << "OpenGL error!";
        hasError = true;
    }

    if(!quiet)
        assert(!hasError && "OpenGL error!");
    return !hasError;
}
