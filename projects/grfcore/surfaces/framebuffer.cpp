

#include "framebuffer.h"
#include "renderbuffer.h"

using namespace grf;

FrameBuffer::FrameBuffer()
{

}

FrameBuffer::~FrameBuffer()
{
    glDeleteFramebuffers(1, &m_id);
}

ptr<RenderBuffer> FrameBuffer::colorBuffer(int idx) const
{
    if((idx < 0) || (idx >= m_colorBuffers.size()))
        return nullptr;

    return m_colorBuffers[idx];
}

void FrameBuffer::addColorBuffer(ptr<RenderBuffer> buffer)
{
    m_colorBuffers.push_back(buffer);
}

void FrameBuffer::setColorBuffer(ptr<RenderBuffer> buffer, int idx)
{
    if((idx < 0) || (idx >= m_colorBuffers.size()))
        return;

    m_colorBuffers[idx] = buffer;
}

ptr<RenderBuffer> FrameBuffer::depthBuffer() const
{
    return m_depthBuffer;
}

ptr<RenderBuffer> FrameBuffer::stencilBuffer() const
{
    return m_stencilBuffer;
}

FrameBuffer::Status FrameBuffer::status() const
{
    return m_status;
}

void FrameBuffer::setStatus(FrameBuffer::Status status)
{
    m_status = status;
}

void FrameBuffer::setStencilBuffer(ptr<RenderBuffer> stencilBuffer)
{
    m_stencilBuffer = stencilBuffer;
}

void FrameBuffer::setDepthBuffer(ptr<RenderBuffer> depthBuffer)
{
    m_depthBuffer = depthBuffer;
}

void FrameBuffer::update(bool force)
{
    if((m_status == kUpdated) && !force)
        return;

    if(!m_id)
    {
        glGenFramebuffers(1, &m_id);
    }

    glBindFramebuffer(GL_FRAMEBUFFER, m_id);

    for(int i = 0; i < m_colorBuffers.size(); i++)
    {
        m_colorBuffers[i]->update(force);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
                                  GL_RENDERBUFFER, m_colorBuffers[i]->id());
    }

    if(m_depthBuffer)
    {
        m_depthBuffer->update(force);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                                  GL_RENDERBUFFER, m_depthBuffer->id());
    }

    if(m_stencilBuffer)
    {
        if(m_stencilBuffer != m_depthBuffer)
            m_stencilBuffer->update(force);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                                  GL_RENDERBUFFER, m_stencilBuffer->id());
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    m_status = kUpdated;
}

void FrameBuffer::bind()
{
    update();
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
}

void FrameBuffer::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
