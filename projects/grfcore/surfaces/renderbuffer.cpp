#include "renderbuffer.h"

using namespace grf;

RenderBuffer::RenderBuffer()
{

}

RenderBuffer::RenderBuffer(int width, int height, GLuint format, GLuint samples) :
    m_width(width), m_height(height), m_format(format), m_samples(samples)
{

}

RenderBuffer::~RenderBuffer()
{
    glDeleteRenderbuffers(1, &m_id);
}

void RenderBuffer::update(bool force)
{
    if((m_status == kUpdated) && !force)
        return;

    if(!m_id)
    {
        glGenRenderbuffers(1, &m_id);
    }

    glBindRenderbuffer(GL_RENDERBUFFER, m_id);
    if(m_samples <= 1)
    {
        glRenderbufferStorage(GL_RENDERBUFFER, m_format, m_width, m_height);
    }
    else
    {
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, m_samples, m_format, m_width, m_height);
    }
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    m_status = kUpdated;
}

void RenderBuffer::bind()
{
    update();
    glBindRenderbuffer(GL_RENDERBUFFER, m_id);
}

void RenderBuffer::unbind()
{
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

GLuint RenderBuffer::id() const
{
    return m_id;
}

int RenderBuffer::width() const
{
    return m_width;
}

int RenderBuffer::height() const
{
    return m_height;
}

GLuint RenderBuffer::format() const
{
    return m_format;
}

GLuint RenderBuffer::samples() const
{
    return m_samples;
}

RenderBuffer::Status RenderBuffer::status() const
{
    return m_status;
}

void RenderBuffer::setStatus(RenderBuffer::Status status)
{
    m_status = status;
}
