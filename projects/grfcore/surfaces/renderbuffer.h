#ifndef RENDERBUFFER_H
#define RENDERBUFFER_H

#include "grfcore/common.h"

namespace grf {

class RenderBuffer : public grf::Object
{
public:
    enum Status {kUpdated, kDirty};
public:
    RenderBuffer();
    RenderBuffer(int width, int height, GLuint format = GL_RGBA8, GLuint samples = 1);
    ~RenderBuffer();

    void update(bool force = false);

    void bind();
    void unbind();

    GLuint id() const;
    int width() const;
    int height() const;
    GLuint format() const;
    GLuint samples() const;

    Status status() const;
    void setStatus(Status status);

private:
    GLuint m_id = 0;
    int m_width = 0;
    int m_height = 0;
    GLuint m_format = 0;
    GLuint m_samples = 0;
    Status m_status = kDirty;
};

} // grf
#endif // RENDERBUFFER_H
