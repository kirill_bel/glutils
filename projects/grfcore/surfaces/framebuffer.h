#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <vector>
#include "grfcore/common.h"

namespace grf {

class RenderBuffer;
class FrameBuffer : public grf::Object
{
public:
    enum Status {kUpdated, kDirty};
public:
    FrameBuffer();
    ~FrameBuffer();

    ptr<RenderBuffer> colorBuffer(int idx) const;
    void addColorBuffer(ptr<RenderBuffer> buffer);
    void setColorBuffer(ptr<RenderBuffer> buffer, int idx);

    ptr<RenderBuffer> depthBuffer() const;
    void setDepthBuffer(ptr<RenderBuffer> depthBuffer);

    ptr<RenderBuffer> stencilBuffer() const;
    void setStencilBuffer(ptr<RenderBuffer> stencilBuffer);

    Status status() const;
    void setStatus(Status status);

    void update(bool force = false);

    void bind();
    void unbind();
private:
    std::vector< ptr<RenderBuffer> > m_colorBuffers;
    ptr<RenderBuffer> m_depthBuffer;
    ptr<RenderBuffer> m_stencilBuffer;
    GLuint m_id = 0;
    Status m_status = kDirty;
};

} // grf
#endif // FRAMEBUFFER_H
