#include "texture.h"

using namespace grf;

Texture::Texture(Target target) :
    m_target(target)
{

}

void Texture::lazyInit()
{
    if(!m_id)
        glGenTextures(1, &m_id);
}


bool Texture::allocate(int width, int height, GLint internalFormat, GLenum format, GLenum type)
{
    lazyInit();

    glBindTexture(m_target, m_id);
    glTexImage2D(m_target, 0, internalFormat, width, height, 0, format,
                     type, nullptr);
    if(!checkOpenGLError())
        return false;

    m_allocated = true;
    m_width = width;
    m_height = height;
    m_internalPixelFormat = internalFormat;
    m_pixelFormat = format;
    m_pixelType = type;

    setStatus(kDirty);

    return true;
}

bool Texture::setData(const uint8_t *ptr, bool generateMipMaps)
{
    lazyInit();

    if(!isAllocated())
    {
        assert(false && "Texture not allocated!");
        return false;
    }

    glBindTexture(m_target, m_id);
    glCopyTexImage2D(m_target, 0, internalPixelFormat(), 0, 0, width(), height(), 0);
    if(!checkOpenGLError())
        return false;

    if(generateMipMaps)
    {
        glGenerateMipmap(m_target);
        if(!checkOpenGLError())
            grfWarning() << "Failed to generate mipmaps!";
    }

    setStatus(kDirty);
    return true;
}

bool Texture::setData(const uint8_t *ptr, int width, int height, GLint internalFormat, GLenum format, GLenum type, bool generateMipMaps)
{
    lazyInit();

    glBindTexture(m_target, m_id);
    glTexImage2D(m_target, 0, internalFormat, width, height, 0, format,
                     type, ptr);
    if(!checkOpenGLError())
        return false;

    if(generateMipMaps)
    {
        glGenerateMipmap(m_target);
        if(!checkOpenGLError())
            grfWarning() << "Failed to generate mipmaps!";
    }

    m_allocated = true;
    m_width = width;
    m_height = height;
    m_internalPixelFormat = internalFormat;
    m_pixelFormat = format;
    m_pixelType = type;

    setStatus(kDirty);
    return true;
}

void Texture::update(bool force)
{
    lazyInit();

    if(!m_allocated)
    {
        assert(false && "Texture not allocated!");
        return;
    }

    glBindTexture(target(), id());
    glTexParameteri(target(), GL_TEXTURE_MIN_FILTER, m_minFilter);
    glTexParameteri(target(), GL_TEXTURE_MAG_FILTER, m_magFilter);
    glTexParameteri(target(), GL_TEXTURE_WRAP_S, m_wrapS);
    glTexParameteri(target(), GL_TEXTURE_WRAP_T, m_wrapT);
    glTexParameteri(target(), GL_TEXTURE_WRAP_R, m_wrapR);
    setStatus(kUpdated);
}

void Texture::bind()
{
    update();
    glBindTexture(target(), id());
}

void Texture::unbind()
{
    glBindTexture(target(), 0);
}

GLuint Texture::id() const
{
    return m_id;
}

Texture::Filter Texture::minFilter() const
{
    return m_minFilter;
}

void Texture::setMinFilter(const Filter &minFilter)
{
    m_minFilter = minFilter;
    setStatus(kDirty);
}

Texture::Filter Texture::magFilter() const
{
    return m_magFilter;
}

void Texture::setMagFilter(const Filter &magFilter)
{
    m_magFilter = magFilter;
    setStatus(kDirty);
}

Texture::Wrap Texture::wrapS() const
{
    return m_wrapS;
}

void Texture::setWrapS(const Wrap &wrapS)
{
    m_wrapS = wrapS;
    setStatus(kDirty);
}

Texture::Wrap Texture::wrapT() const
{
    return m_wrapT;
}

void Texture::setWrapT(const Wrap &wrapT)
{
    m_wrapT = wrapT;
    setStatus(kDirty);
}

Texture::Wrap Texture::wrapR() const
{
    return m_wrapR;
}

void Texture::setWrapR(const Wrap &wrapR)
{
    m_wrapR = wrapR;
    setStatus(kDirty);
}

Texture::Status Texture::status() const
{
    return m_status;
}

void Texture::setStatus(const Status &status)
{
    m_status = status;
}

size_t Texture::width() const
{
    return m_width;
}

size_t Texture::height() const
{
    return m_height;
}

Texture::Target Texture::target() const
{
    return m_target;
}

bool Texture::isAllocated() const
{
    return m_allocated;
}

GLint Texture::internalPixelFormat() const
{
    return m_internalPixelFormat;
}

GLenum Texture::pixelFormat() const
{
    return m_pixelFormat;
}

GLenum Texture::pixelType() const
{
    return m_pixelType;
}
