#ifndef TEXTURE_H
#define TEXTURE_H

#include "grfcore/common.h"

namespace grf {

class Texture
{    
    void lazyInit();
public:
    enum Status {kUpdated, kDirty};
    enum Target
    {
        kTexture2d =                GL_TEXTURE_2D,
        kProxyTexture2d =           GL_PROXY_TEXTURE_2D,
        kTexture1d =                GL_TEXTURE_1D_ARRAY,
        kProxyTexture1d =           GL_PROXY_TEXTURE_1D_ARRAY,
        kTextureRectangle =         GL_TEXTURE_RECTANGLE,
        kProxyTextureRectangle =    GL_PROXY_TEXTURE_RECTANGLE,
        kCubeMapPosX =              GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        kCubeMapNegX =              GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        kCubeMapPoxY =              GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
        kCubeMapNegY =              GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        kCubeMapPoxZ =              GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
        kCubeMapNegZ =              GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
        kProxyCubeMap =             GL_PROXY_TEXTURE_CUBE_MAP
    };

    enum Filter
    {
        kNearest =                  GL_NEAREST,
        kLinear =                   GL_LINEAR,
        kNearestMipNearest =        GL_NEAREST_MIPMAP_NEAREST,
        kLinearMipNearest =         GL_LINEAR_MIPMAP_NEAREST,
        kNearestMipLinear =         GL_NEAREST_MIPMAP_LINEAR,
        kLinearMipLinear =          GL_LINEAR_MIPMAP_LINEAR
    };

    enum Wrap
    {
        kClampToEdge =              GL_CLAMP_TO_EDGE,
        kClampToBorder =            GL_CLAMP_TO_BORDER,
        kMirroredRepeat =           GL_MIRRORED_REPEAT,
        kRepeat =                   GL_REPEAT,
        kMirrorClampToEdge =        GL_MIRROR_CLAMP_TO_EDGE
    };
public:
    Texture(Target target = kTexture2d);
//    Texture(size_t width, size_t height, );
    Texture(const Texture& other) = delete;

    bool allocate(int width, int height, GLint internalFormat = GL_RGBA8, GLenum format = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE);
    bool setData(const uint8_t* ptr, bool generateMipMaps = true);
    bool setData(const uint8_t* ptr, int width, int height, GLint internalFormat = GL_RGBA8, GLenum format = GL_RGBA, GLenum type = GL_UNSIGNED_BYTE, bool generateMipMaps = true);
    void update(bool force = false);
    void release();

    void bind();
    void unbind();

    GLuint id() const;

    Filter minFilter() const;
    void setMinFilter(const Filter &minFilter);

    Filter magFilter() const;
    void setMagFilter(const Filter &magFilter);

    Wrap wrapS() const;
    void setWrapS(const Wrap &wrapS);

    Wrap wrapT() const;
    void setWrapT(const Wrap &wrapT);

    Wrap wrapR() const;
    void setWrapR(const Wrap &wrapR);

    Status status() const;
    void setStatus(const Status &status);

    size_t width() const;
    size_t height() const;

    Target target() const;

    bool isAllocated() const;

    GLint internalPixelFormat() const;

    GLenum pixelFormat() const;

    GLenum pixelType() const;

private:
    size_t m_width = 0;
    size_t m_height = 0;

    GLint m_internalPixelFormat = 0;
    GLenum m_pixelFormat = 0;
    GLenum m_pixelType = 0;

    Target m_target = kTexture2d;
    Filter m_minFilter = kLinearMipLinear;
    Filter m_magFilter = kLinear;
    Wrap m_wrapS = kClampToEdge;
    Wrap m_wrapT = kClampToEdge;
    Wrap m_wrapR = kClampToEdge;
    Status m_status = kDirty;

    GLuint m_id = 0;
    bool m_allocated = false;
};

} // grf

#endif // TEXTURE_H
