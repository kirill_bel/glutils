#ifndef BASICTESTS_H
#define BASICTESTS_H

#include <QObject>
#include <QTest>

class BasicTests : public QObject
{
    Q_OBJECT
public:
    BasicTests();

private slots:
    void initTestCase();
    void entityAddComponentsTest();
    void sceneComponentAttachComponentsTest();
    void sceneComponentTransformTest();
};

#endif // BASICTESTS_H
