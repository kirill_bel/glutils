#include <grf/common.h>
#include <QCoreApplication>
#include <QTest>

#include "basictests.h"

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QTest::qExec(new BasicTests(), argc, argv);

    return 1;
}
