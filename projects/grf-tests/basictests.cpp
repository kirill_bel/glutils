#include "basictests.h"

#include <grf/entity/entity.h>
#include <grf/component/entitycomponent.h>
#include <grf/component/scenecomponent.h>

BasicTests::BasicTests()
{

}

void BasicTests::initTestCase()
{

}

void BasicTests::entityAddComponentsTest()
{
    QScopedPointer<grf::Entity> entity1(new grf::Entity());
    QScopedPointer<grf::Entity> entity2(new grf::Entity());

    grf::EntityComponent* comp1 = new grf::EntityComponent(entity1.data());

    QVERIFY(comp1->getOwner() == entity1.data());
    QVERIFY(entity1->hasComponent(comp1));

    comp1->setParent(entity2.data());

    QVERIFY(comp1->getOwner() == entity2.data());
    QVERIFY(entity2->hasComponent(comp1));
    QVERIFY(!entity1->hasComponent(comp1));

    delete comp1;

    QVERIFY(!entity2->hasComponent(comp1));
}

void BasicTests::sceneComponentAttachComponentsTest()
{
    QScopedPointer<grf::Entity> entity(new grf::Entity());

    grf::SceneComponent* rootComp1 = new grf::SceneComponent(entity.data());
    grf::SceneComponent* rootComp2 = new grf::SceneComponent(entity.data());

    grf::SceneComponent* comp1 = new grf::SceneComponent(entity.data());

    QVERIFY(comp1->attachToComponent(rootComp1));
    QVERIFY(comp1->isAttachedTo(rootComp1));
    QVERIFY(rootComp1->isComponentAttached(comp1));
    QVERIFY(rootComp1->getAttachedChildren().contains(comp1));

    QVERIFY(comp1->attachToComponent(rootComp2));
    QVERIFY(comp1->isAttachedTo(rootComp2));
    QVERIFY(!rootComp1->isComponentAttached(comp1));
    QVERIFY(rootComp2->isComponentAttached(comp1));
    QVERIFY(!rootComp1->getAttachedChildren().contains(comp1));
    QVERIFY(rootComp2->getAttachedChildren().contains(comp1));

    QVERIFY(!rootComp2->attachToComponent(rootComp2));

    QVERIFY(rootComp1->attachToComponent(rootComp2));
    QVERIFY(!rootComp2->attachToComponent(rootComp2));

    delete rootComp2;

    QVERIFY(rootComp1->getAttachedChildren().empty());
}

void BasicTests::sceneComponentTransformTest()
{
    QScopedPointer<grf::Entity> entity(new grf::Entity());

    grf::SceneComponent* rootComp = new grf::SceneComponent(entity.data());

    grf::SceneComponent* comp1 = new grf::SceneComponent(entity.data());
    comp1->attachToComponent(rootComp);

    grf::SceneComponent* comp2 = new grf::SceneComponent(entity.data());
    comp2->attachToComponent(comp1);

    rootComp->transform().p.x = 10;
    rootComp->updateTransform();

    QVERIFY(comp1->worldTransform().p.x == 10);
    QVERIFY(comp2->worldTransform().p.x == 10);

    comp1->transform().p.x = 20;
    comp1->updateTransform();

    QVERIFY(comp2->worldTransform().p.x == 30);

    comp1->detachFromComponent(rootComp);
    comp1->updateTransform();
    QVERIFY(comp1->worldTransform().p.x == 20);
    QVERIFY(comp2->worldTransform().p.x == 20);

    delete comp2;

    comp1->updateTransform();
}
