//uniform sampler2D qt_Texture0;
varying vec4 qt_FragColor;

void main(void)
{
    gl_FragColor = vec4(qt_FragColor.xyz,1);
}
