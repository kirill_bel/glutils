attribute vec4 qt_Vertex;
attribute vec4 qt_VertexColor;
//attribute vec4 qt_MultiTexCoord0;
uniform mat4 qt_ModelViewProjectionMatrix;
varying vec4 qt_FragColor;

void main(void)
{
    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;
    qt_FragColor = qt_VertexColor;
//    qt_TexCoord0 = qt_MultiTexCoord0;
}
