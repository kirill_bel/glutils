#ifndef CSSPREPROCESSOR_H
#define CSSPREPROCESSOR_H

#include <QString>
#include <QVariantMap>
#include <functional>

class CssPreprocessor
{
public:
    using PropertyWriter = std::function<bool(const QVariant& inProp, QString& outProp)>;
    using PropertyReader = std::function<bool(const QString& inProp, QVariant& outProp)>;
public:
    CssPreprocessor(const QString& file);

    void registerPropertyType(QVariant::Type propertyType, QString typeName);
    void registerPropertyHandler(QVariant::Type propertyType, PropertyWriter writer = nullptr, PropertyReader reader = nullptr);

    QVariant property(const QString& name);
    void setProperty(const QString& name, const QVariant& value);

    bool process();

    QString getProcessedResult() const;

protected:
    bool processTokens(const QStringList& tokens);
    QVariant::Type propertyType(const QString& name) const;
    PropertyReader readerForType(const QString& type) const;
    PropertyWriter writerForType(QVariant::Type typeId) const;
    QStringList tokenize(const QString& line);
private:
    QVariantMap m_properties;
    QMap<QString, QVariant::Type> m_propNameToType;
    QMap<QVariant::Type, PropertyReader> m_propReaders;
    QMap<QVariant::Type, PropertyWriter> m_propWriters;

    QString m_filename;
    QString m_cssSource;
    QString m_cssProcessed;

};

#endif // CSSPREPROCESSOR_H
