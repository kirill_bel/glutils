#include "csspreprocessor.h"
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <cctype>
#include <QColor>

CssPreprocessor::CssPreprocessor(const QString& file)
{
    QFile f(file);
    if (f.exists())
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        m_cssSource = ts.readAll();
        f.close();
    }

    registerPropertyType(QVariant::Color, "color");
    registerPropertyType(QVariant::ByteArray, "define");

    registerPropertyHandler(QVariant::Color,
                            [](const QVariant& inProp, QString& outProp){
        outProp = inProp.value<QColor>().name(QColor::HexArgb);
        return true;
    },
    [](const QString& inProp, QVariant& outProp){
        outProp = QVariant::fromValue(QColor(inProp));
        return true;
    });

    registerPropertyHandler(QVariant::ByteArray,
                            [](const QVariant& inProp, QString& outProp){
        outProp = QString(inProp.toByteArray());
        return true;
    },
    [](const QString& inProp, QVariant& outProp){
        outProp = QVariant(inProp.toUtf8());
        return true;
    });
}

void CssPreprocessor::registerPropertyType(QVariant::Type propertyType, QString typeName)
{
    m_propNameToType.insert(typeName, propertyType);
}

void CssPreprocessor::registerPropertyHandler(QVariant::Type propertyType,
                                              CssPreprocessor::PropertyWriter writer,
                                              CssPreprocessor::PropertyReader reader)
{
    if(writer)
        m_propWriters[propertyType] = writer;
    if(reader)
        m_propReaders[propertyType] = reader;
}

QVariant CssPreprocessor::property(const QString &name)
{
    auto it = m_properties.find(name);
    if(it == m_properties.end())
        return QVariant();
    return it.value();
}

void CssPreprocessor::setProperty(const QString &name, const QVariant &value)
{
    m_properties[name] = value;
}

bool CssPreprocessor::process()
{
    if(m_cssSource.isEmpty())
        return false;

    m_cssProcessed.clear();

    QTextStream in(&m_cssSource, QIODevice::ReadOnly);

    while(!in.atEnd())
    {
        QString line = in.readLine();

        int startIdx = line.size();
        int startIdx1 = line.indexOf('@');
        int startIdx2 = line.indexOf("//");
        if(startIdx1 != -1)
            startIdx = startIdx1;
        if(startIdx2 != -1)
            startIdx = std::min(startIdx, startIdx2);

        QString lineClean = line.left(startIdx);
        QString lineShifted = line.right(line.size() - startIdx);

        m_cssProcessed += lineClean + "\n";

        QStringList tokens = tokenize(lineShifted);
        if(!tokens.empty())
        {
            if(tokens.first().startsWith('@'))
            {
                if(tokens.first().length() == 1)
                    tokens.removeFirst();
                else
                    tokens.first().remove(0, 1);

                if(!processTokens(tokens))
                {
                    qWarning() << "Failed to process tokens:" << line;
                    m_cssProcessed += line;
                }
            }
            else if(tokens.first().startsWith("//"))
            {
                // Skip comment
            }
            else
            {
                m_cssProcessed += lineShifted;
            }
        }
    }


    for(auto it = m_properties.begin(); it != m_properties.end(); it++)
    {
        PropertyWriter writer = writerForType(it.value().type());
        if(!writer)
        {
            qWarning() << "Failed to find writer for property" << it.key() << "and value" << it.value();
            continue;
        }

        QString strValue;
        if(!writer(it.value(), strValue))
        {
            qWarning() << "Failed to convert property:" << it.key() << it.value();
            continue;
        }

        m_cssProcessed.replace(it.key(), strValue);
    }


    return true;
}

QString CssPreprocessor::getProcessedResult() const
{
    return m_cssProcessed;
}

QStringList CssPreprocessor::tokenize(const QString &line)
{
    QStringList tokens;
    QByteArray buf = line.toLocal8Bit();

    const char* begin = buf.data();
    const char* cur = begin;
    const char* end = begin + buf.size();

    while((cur < end) && std::isspace(*cur))
        cur++;

    if(*cur != '@')
        return tokens;

    bool isValue = false;
    while(cur < end)
    {
        while((cur < end) && std::isspace(*cur))
            cur++;

        if(cur >= end)
            break;

        if(isValue)
        {
            QString data(*cur); cur++;
            while((cur < end) && (*cur != ';'))
            {
                data += *cur;
                cur++;
            }

            tokens.append(data);
            isValue = false;
        }

        const char* str = cur;
        if(std::isalpha(*cur))
        {
            QString data(*str);
            str++;

            while(std::isalnum(*str) || (*str == '_'))
            {
                data += *str;
                str++;
            }
            cur = str;
            tokens.append(data);
            continue;
        }

        if(cur < end)
        {
            switch(*cur)
            {
            case '=':
                isValue = true;
            case '@':
            case ';':
                tokens.append(QString(*cur));
                cur++;
                continue;
            }

        }

        if((cur + 1) < end)
        {
            if((*cur == '/') && (*(cur+1) == '/'))
            {
                tokens.append(QString(cur));
                cur = end;
                continue;
            }
        }

        qWarning() << "Unknown lexeme:" << *cur;
        cur++;

    }

    return tokens;
}

bool CssPreprocessor::processTokens(const QStringList &tokens)
{
    if(tokens.first() == "property")
    {
        if((tokens.size() == 6) && (tokens.at(3) == "=") && (tokens.at(5) == ";"))
        {
            PropertyReader reader = readerForType(tokens.at(1));
            QVariant prop;
            if(reader && reader(tokens.at(4), prop) && prop.isValid())
                setProperty(tokens.at(2), prop);
            else
            {
                qWarning() << "Failed to read property!";
                return false;
            }

            qDebug() << "Tokens:" << tokens;
        }
        else if((tokens.size() == 5) && (tokens.at(2) == "=") && (tokens.at(4) == ";"))
        {
            PropertyReader reader = readerForType("define");
            QVariant prop;

            if(reader && reader(tokens.at(3), prop) && prop.isValid())
                setProperty(tokens.at(1), prop);
            else
            {
                qWarning() << "Failed to read property!";
                return false;
            }

            qDebug() << "Tokens:" << tokens;
        }
        else
        {
            qWarning() << "Incorrect token format for property!";
            return false;
        }
    }
    else
        return false;
    return true;
}

QVariant::Type CssPreprocessor::propertyType(const QString &name) const
{
    auto it = m_propNameToType.find(name);
    if(it == m_propNameToType.end())
        return QVariant::Invalid;

    return it.value();
}

CssPreprocessor::PropertyReader CssPreprocessor::readerForType(const QString &type) const
{
    QVariant::Type typeId = propertyType(type);
    if(typeId == QVariant::Invalid)
        return nullptr;

    auto it = m_propReaders.find(typeId);
    if(it == m_propReaders.end())
        return nullptr;

    return it.value();
}

CssPreprocessor::PropertyWriter CssPreprocessor::writerForType(QVariant::Type typeId) const
{
    auto it = m_propWriters.find(typeId);
    if(it == m_propWriters.end())
        return nullptr;

    return it.value();
}
