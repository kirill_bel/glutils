#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <grf/ui/renderwidget.h>
#include <grf/entity/inputcontroller.h>
#include <grf/entity/cameraentity.h>

#include <QTextEdit>

#include <QDebug>
#include <QVBoxLayout>

MainWindow::MainWindow(std::unique_ptr<CssPreprocessor> css, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setCss(std::move(css));

    grf::log::Context::SetLogCallback([](grf::log::Level level,
                                      const grf::log::Context& context,
                                      const std::string& str)
    {
        std::ostream* stream = &(std::cout);
        switch(level)
        {
        case grf::log::kError:
            stream = &(std::cerr);
            (*stream) << "Error(" << context.function << "): ";
            break;
        case grf::log::kWarning:
            stream = &(std::cerr);
            (*stream) << "Warning(" << context.function << "): ";
            break;
        case grf::log::kInfo:
            break;
        case grf::log::kDebug:
            break;
        }

        (*stream) << str.c_str();
        (*stream).flush();
    });

    m_doc = new ds::Document(this);
    m_doc->addModel(new ds::WindowModel());
    m_doc->getModel<ds::WindowModel>()->mainWindow = this;

    m_presenter = new ds::Presenter(this);
    m_presenter->setController(new ds::WindowCtrl());
    m_presenter->setDocument(m_doc);

    initDockableWidgets();
    initViews();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setCss(std::unique_ptr<CssPreprocessor> css)
{
    m_css = std::move(css);
}

void MainWindow::initDockableWidgets()
{
    QDockWidget* sceneDockWidget = new QDockWidget("Scene", this);
    sceneDockWidget->setWidget(new QWidget());
    sceneDockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
    sceneDockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    this->addDockWidget(Qt::LeftDockWidgetArea, sceneDockWidget);

    QDockWidget* propertiesDockWidget = new QDockWidget("Properties", this);
    propertiesDockWidget->setWidget(new QWidget());
    propertiesDockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
    propertiesDockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    this->addDockWidget(Qt::RightDockWidgetArea, propertiesDockWidget);

    QDockWidget* logDockWidget = new QDockWidget("Log", this);
    logDockWidget->setWidget(new QWidget());
    logDockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
    logDockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    this->addDockWidget(Qt::BottomDockWidgetArea, logDockWidget);

    QDockWidget* assetsDockWidget = new QDockWidget("Assets", this);
    assetsDockWidget->setWidget(new QWidget());
    assetsDockWidget->setAllowedAreas(Qt::AllDockWidgetAreas);
    assetsDockWidget->setFeatures(QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
    this->addDockWidget(Qt::BottomDockWidgetArea, assetsDockWidget);

    this->tabifyDockWidget(logDockWidget, assetsDockWidget);
    this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    this->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    m_presenter->getController<ds::WindowCtrl>()->registerDockingWidget(sceneDockWidget);
    m_presenter->getController<ds::WindowCtrl>()->registerDockingWidget(propertiesDockWidget);
    m_presenter->getController<ds::WindowCtrl>()->registerDockingWidget(logDockWidget);
    m_presenter->getController<ds::WindowCtrl>()->registerDockingWidget(assetsDockWidget);

    ui->menuView->insertMenu(nullptr, m_doc->getModels<ds::WindowModel>().first()->dockWidgetsMenu);
}

void MainWindow::initViews()
{
    grf::RenderWidget* widget = new grf::RenderWidget();
    widget->setObjectName("Scene View");
    widget->setGridSize(QSize(2,2));
    widget->setForegroundColor(m_css->property("BACKGROUND_COLOR").value<QColor>());

    int colorInd = (int)Qt::white + 1;
    for(int x = 0; x < 2; x++)
    {
        for(int y = 0; y < 2; y++)
        {
            if(colorInd < (int)Qt::transparent)
            {
                grf::Viewport* vp = new grf::Viewport(QColor((Qt::GlobalColor)colorInd++), widget);

                grf::InputController* inputCtrl = new grf::InputController(this);

                grf::CameraEntity* camera = new grf::CameraEntity(vp);
                camera->getRootComponent()->transform().set(glm::vec3(0,0,-10));
                camera->getRootComponent()->updateTransform();
                camera->enableInput(inputCtrl);

                vp->setCameraComponent(camera->getComponents<grf::CameraComponent>().first());
                vp->setInputController(inputCtrl);

                widget->insertViewport(vp, x,y);
            }
        }
    }

    QTextEdit* editorWidget = new QTextEdit();
    editorWidget->setObjectName("Text editor");

    m_presenter->getController<ds::WindowCtrl>()->registerView(widget);
    m_presenter->getController<ds::WindowCtrl>()->registerView(editorWidget);
}
