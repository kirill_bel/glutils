#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include "stylesheets/csspreprocessor.h"

#include <docshell/library/windowctrl.h>
#include <docshell/library/windowmodel.h>
#include <docshell/presenter/presenter.h>
#include <docshell/document/document.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(std::unique_ptr<CssPreprocessor> css, QWidget *parent = 0);
    ~MainWindow();

    void setCss(std::unique_ptr<CssPreprocessor> css);

protected:
    void initDockableWidgets();
    void initViews();
private:
    Ui::MainWindow *ui;
    ds::Document* m_doc;
    ds::Presenter* m_presenter;
    std::unique_ptr<CssPreprocessor> m_css;
};

#endif // MAINWINDOW_H
