#include "mainwindow.h"
#include <QApplication>

#include <QSurfaceFormat>
#include <QFile>
#include <QTextStream>
#include <QDebug>

#include <iostream>

int main(int argc, char *argv[])
{
    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    format.setSamples(8);
#ifndef NODEBUG
    format.setOption(QSurfaceFormat::DebugContext);
#endif
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CompatibilityProfile);
    format.setVersion(4,0);
    QSurfaceFormat::setDefaultFormat(format);


    QApplication a(argc, argv);
    Q_INIT_RESOURCE(framework);

    std::unique_ptr<CssPreprocessor> preproc(new CssPreprocessor(":qdarkstyle/style.qss"));

    if(!preproc->process())
        qCritical() << "Failed to process properties!";
    else
        qApp->setStyleSheet(preproc->getProcessedResult());

    QFile fileTemp("updated_stylesheet.txt");
    fileTemp.open(QIODevice::WriteOnly);
    fileTemp.write(preproc->getProcessedResult().toUtf8());
    fileTemp.close();


    MainWindow w(std::move(preproc));
    w.show();

    return a.exec();
}
