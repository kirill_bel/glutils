
cmake_minimum_required(VERSION 3.4.1)

project(GrfApplication)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

############### DEPENDENCIES #####################

set(GRFAPP_INCLUDE_DIRS)
set(GRFAPP_LIBRARIES)
set(GRFAPP_DEFINITIONS)

set(Qt5Modules Core Concurrent Test OpenGL Widgets)
find_package(Qt5 COMPONENTS ${Qt5Modules} REQUIRED)

foreach(QtModule IN LISTS Qt5Modules)
	list(APPEND GRFAPP_LIBRARIES Qt5::${QtModule})
endforeach(QtModule)

set(CMAKE_AUTOMOC ON)

############## ~DEPENDENCIES #####################

file(GLOB_RECURSE SOURCE_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)
file(GLOB_RECURSE RC_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.qrc)
file(GLOB_RECURSE UI_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.ui)

qt5_wrap_ui(QUI ${UI_FILES})
qt5_add_resources(QRCS ${RC_FILES})

add_executable(grf_app ${SOURCE_FILES} ${QUI} ${QRCS})
set_target_properties(grf_app PROPERTIES OUTPUT_NAME "grfapp")

target_link_libraries(grf_app 
        grf_library
        docshell_library
	${GRFAPP_LIBRARIES}
	)

install(TARGETS grf_app
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

# Use cotire in current project
set_target_properties(grf_app PROPERTIES COTIRE_ADD_UNITY_BUILD FALSE)
set_target_properties(grf_app PROPERTIES COTIRE_ENABLE_PRECOMPILED_HEADER FALSE)
cotire(grf_app)
