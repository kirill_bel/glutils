#include "windowctrl.h"
#include "windowmodel.h"

using namespace ds;

WindowCtrl::WindowCtrl(Presenter *parent) :
    Controller(parent)
{

}

void WindowCtrl::registerDockingWidget(QDockWidget *dockWidget)
{
    assert(m_windowModel);

    if(m_windowModel->dockWidgets.contains(dockWidget))
        return;

    m_windowModel->dockWidgets.insert(dockWidget);
    m_windowModel->dockWidgetsMenu->insertAction(0, dockWidget->toggleViewAction());
}

void WindowCtrl::registerToolbar(QToolBar *toolbar)
{
    assert(m_windowModel);
    m_windowModel->toolbars.insert(toolbar);
}

void WindowCtrl::registerView(QWidget *widget)
{
    assert(m_windowModel);
    m_windowModel->views.insert(widget);

    m_windowModel->centralWidget->addTab(widget, widget->objectName());
}

void WindowCtrl::onDocumentChanged(Document *doc)
{
    m_windowModel = nullptr;
    if(doc)
    {
        auto models = doc->getModels<WindowModel>();
        if(models.empty())
        {
            assert(models.empty() && "Document not containing window model!");
        }
        else
            m_windowModel = models.front();
    }

    if(m_windowModel)
    {
        if(!m_windowModel->centralWidget)
        {
            m_windowModel->centralWidget = new QTabWidget();
            m_windowModel->centralWidget->setDocumentMode(true);
            m_windowModel->centralWidget->setMovable(true);
            m_windowModel->centralWidget->setTabsClosable(true);
            m_windowModel->mainWindow->setCentralWidget(m_windowModel->centralWidget);
        }        
    }
}
