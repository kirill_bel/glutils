#ifndef WINDOWMODEL_H
#define WINDOWMODEL_H

#include "docshell/document/model.h"

namespace ds {

class WindowModel : public Model
{
    Q_OBJECT
public:
    explicit WindowModel(Document *parent = nullptr);

    QMainWindow* mainWindow = nullptr;
    QSet<QDockWidget*> dockWidgets;
    QSet<QToolBar*> toolbars;
    QSet<QWidget*> views;

    QMenu* dockWidgetsMenu = nullptr;
    QMenu* viewsMenu = nullptr;

    QTabWidget* centralWidget = nullptr;
};

} // ds

#endif // WINDOWMODEL_H
