#include "windowmodel.h"

using namespace ds;

WindowModel::WindowModel(Document *parent) : Model(parent)
{
    dockWidgetsMenu = new QMenu("Windows");
    viewsMenu = new QMenu("Views");
}
