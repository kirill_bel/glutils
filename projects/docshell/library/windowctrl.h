#ifndef WINDOWCTRL_H
#define WINDOWCTRL_H

#include "docshell/presenter/controller.h"

namespace ds {

class WindowModel;
class WindowCtrl : public Controller
{
    Q_OBJECT
public:
    explicit WindowCtrl(Presenter *parent = nullptr);

    void registerDockingWidget(QDockWidget* dockWidget);
    void registerToolbar(QToolBar* toolbar);
    void registerView(QWidget* widget);


protected:
    void onDocumentChanged(Document* doc) override;
private:
    WindowModel* m_windowModel = nullptr;
};

} // ds

#endif // WINDOWCTRL_H
