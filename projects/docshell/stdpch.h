#ifndef DOCSHELL_STDPCH_H
#define DOCSHELL_STDPCH_H

#include <array>
#include <assert.h>

#include <QSet>
#include <QVector>
#include <QList>
#include <QFile>
#include <QDebug>
#include <QMouseEvent>
#include <QColor>
#include <QTimer>
#include <QMainWindow>
#include <QActionGroup>
#include <QDockWidget>
#include <QMenuBar>

#endif // DOCSHELL_STDPCH_H
