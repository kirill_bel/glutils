#include "presenter.h"
#include "controller.h"

using namespace ds;

Presenter::Presenter(QObject *parent) : QObject(parent)
{

}

void Presenter::setDocument(Document *doc)
{
    if(doc != m_document)
    {
        m_document = doc;

        for(Controller* ctrl : m_controllers)
            ctrl->onDocumentChanged(m_document);

        emit documentChanged();
    }
}

Document *Presenter::document() const
{
    return m_document;
}

void Presenter::setController(Controller *comp)
{
    comp->setParent(this);
}

void Presenter::childEvent(QChildEvent *event)
{
    QObject *childObject = event->child();
    bool isController = childObject->property("ControllerType").toBool();

    Controller* controllerObject = static_cast<Controller*>(childObject);

    if(isController && event->added())
    {
        const char* id = controllerObject->metaObject()->className();
        auto it = m_controllers.find(id);
        if(it != m_controllers.end())
            delete it.value();

        m_controllers[id] = controllerObject;
        controllerObject->onRegister(this);
        controllerObject->onDocumentChanged(m_document);
        emit modifyed();
    }
    else if(event->removed())
    {
        const char* id = controllerObject->metaObject()->className();
        auto it = m_controllers.find(id);
        if(it != m_controllers.end())
        {
            assert((controllerObject == it.value()) && "Incorrect controller!");
            controllerObject->onDocumentChanged(nullptr);
            controllerObject->onUnregister(this);
            it = m_controllers.erase(it);
            emit modifyed();
        }
        else
            assert(false && "Can't find controller!");
    }
}

