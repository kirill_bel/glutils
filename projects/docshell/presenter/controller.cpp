#include "controller.h"
#include "presenter.h"

using namespace ds;

Controller::Controller(Presenter *parent)
{
    this->setParent(parent);
}

void Controller::onRegister(Presenter *presenter)
{
    Q_UNUSED(presenter);
    assert(!m_registered);
    m_registered = true;
}

void Controller::onUnregister(Presenter *presenter)
{
    Q_UNUSED(presenter);
    assert(m_registered);
    m_registered = false;
}

void Controller::onDocumentChanged(Document *doc)
{
    Q_UNUSED(doc);
}
