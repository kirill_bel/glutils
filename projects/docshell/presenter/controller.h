#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "docshell/stdpch.h"
#include "docshell/document/document.h"

namespace ds {

class Presenter;
class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ControllerType MEMBER m_controllerType CONSTANT FINAL)
public:
    explicit Controller(Presenter *parent = nullptr);

    Presenter* getPresenter() const;

protected:
    friend class Presenter;
    virtual void onRegister(Presenter* presenter);
    virtual void onUnregister(Presenter* presenter);
    virtual void onDocumentChanged(Document* doc);
private:
    const bool m_controllerType = true;
    bool m_registered = false;
};

} // ds

#endif // CONTROLLER_H
