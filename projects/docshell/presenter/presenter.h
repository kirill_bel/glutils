#ifndef PRESENTER_H
#define PRESENTER_H

#include "docshell/stdpch.h"
#include "docshell/document/document.h"

namespace ds {

class Controller;
class Presenter : public QObject
{
    Q_OBJECT
public:
    explicit Presenter(QObject *parent = nullptr);

    void setDocument(Document* doc);
    Document *document() const;

    void setController(Controller* comp);

    template<typename T>
    void removeController()
    {
        Controller* ctrl = takeController<T>();
        if(ctrl)
            delete ctrl;
    }

    template<typename T>
    T* takeController()
    {
        T* ctrl = getController<T>();
        if(ctrl)
            ctrl->setParent(nullptr);
        return ctrl;
    }

    template<typename T>
    bool hasController() const
    {
        return getController<T>() != nullptr;
    }

    template<typename T>
    T* getController() const
    {
        const char* id = T::staticMetaObject.className();
        auto it = m_controllers.find(id);
        if(it == m_controllers.end())
            return nullptr;
        return static_cast<T*>(it.value());
    }

signals:
    void modifyed();
    void documentChanged();
private:
    void childEvent(QChildEvent *event) override;
private:
    QMap<const char*, Controller*> m_controllers;
    Document* m_document = nullptr;
};

} // ds

#endif // PRESENTER_H
