#ifndef MODEL_H
#define MODEL_H

#include "docshell/stdpch.h"

namespace ds {

class Document;
class Model : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ModelType MEMBER m_modelType CONSTANT FINAL)
public:
    explicit Model(Document *parent = nullptr);

    Document* getDocument() const;
private:
    const bool m_modelType = true;
};

} // ds

#endif // MODEL_H
