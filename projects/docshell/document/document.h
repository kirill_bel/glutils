#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "docshell/stdpch.h"

namespace ds {

class Model;
class Document : public QObject
{
    Q_OBJECT
public:
    explicit Document(QObject *parent = nullptr);

    void addModel(Model* comp);
    void removeModel(Model* comp);
    Model* takeModel(Model* comp);

    bool hasModel(Model* comp) const;

    template<typename T>
    T* getModel() const
    {
        for(Model* comp : m_models)
            if(qobject_cast<T*>(comp))
                return static_cast<T*>(comp);
        return nullptr;
    }

    template<typename T>
    QList<T*> getModels() const
    {
        QList<T*> list;
        for(Model* comp : m_models)
            if(qobject_cast<T*>(comp))
                list.append(static_cast<T*>(comp));
        return list;
    }

signals:
    void modifyed();
private:
    void childEvent(QChildEvent *event) override;
private:
    QSet<Model*> m_models;
};

} // ds

#endif // DOCUMENT_H
