#include "model.h"
#include "document.h"

using namespace ds;

Model::Model(Document *parent)
{
    this->setParent(parent);
}

Document *Model::getDocument() const
{
    return qobject_cast<Document*>(parent());
}
