#include "document.h"
#include "model.h"

using namespace ds;

Document::Document(QObject *parent) : QObject(parent)
{

}

void Document::addModel(Model *comp)
{
    comp->setParent(this);
}

void Document::removeModel(Model *comp)
{
    if(takeModel(comp))
    {
        delete comp;
    }
}

Model *Document::takeModel(Model *comp)
{
    if(comp->getDocument() != this)
    {
        assert(false && "Incorrect model document!");
        return nullptr;
    }

    comp->setParent(nullptr);
    return comp;
}

bool Document::hasModel(Model *comp) const
{
    return m_models.contains(comp);
}

void Document::childEvent(QChildEvent *event)
{
    QObject *childObject = event->child();
    bool isModel = childObject->property("ModelType").toBool();

    Model* modelObject = static_cast<Model*>(childObject);

    if(isModel && event->added())
    {
#ifdef DEBUG_CHECKS
        assert(!m_models.contains(modelObject));
#endif
        m_models.insert(modelObject);
        emit modifyed();
    }
    else if(event->removed())
    {
        if(m_models.contains(modelObject))
        {
            m_models.remove(modelObject);
            emit modifyed();
        }
    }
}
