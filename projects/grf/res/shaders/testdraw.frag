//uniform sampler2D qt_Texture0;
//varying vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_FragColor = texture2D(qt_Texture0, qt_TexCoord0.st);
//}

uniform sampler2D qt_Texture0;
varying vec4 qt_FragColor;
varying vec2 qt_FragTC;

void main(void)
{
//    gl_FragColor = qt_FragColor;
    gl_FragColor = texture2D(qt_Texture0, qt_FragTC.xy) + qt_FragColor * 0.000001;
//    gl_FragColor = qt_FragColor * 0.00001 + vec4(qt_FragTC.xy, 0, 0);
}
