attribute vec4 qt_Vertex;
attribute vec4 qt_VertexColor;
uniform vec4 qt_AmbientColor;
uniform mat4 qt_WorldViewProjectionMatrix;
varying vec4 qt_FragColor;

void main(void)
{
    gl_Position = (qt_WorldViewProjectionMatrix) * vec4(qt_Vertex.xyz, 1);
    qt_FragColor = qt_VertexColor + qt_AmbientColor;
}
