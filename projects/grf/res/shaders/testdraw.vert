//attribute vec4 qt_Vertex;
//attribute vec4 qt_MultiTexCoord0;
//uniform mat4 qt_ModelViewProjectionMatrix;
//varying vec4 qt_TexCoord0;

//void main(void)
//{
//    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;
//    qt_TexCoord0 = qt_MultiTexCoord0;
//}

attribute vec4 qt_Vertex;
attribute vec2 qt_Texcoord;
attribute vec4 qt_VertexColor;

uniform vec4 qt_AmbientColor;
uniform mat4 qt_WorldMatrix;
uniform mat4 qt_WorldViewProjectionMatrix;
varying vec4 qt_FragColor;
varying vec2 qt_FragTC;

void main(void)
{
    gl_Position = qt_WorldViewProjectionMatrix * vec4(qt_Vertex.xyz, 1);
    qt_FragColor = qt_VertexColor + qt_AmbientColor;
    qt_FragTC = qt_Texcoord;
}
