#ifndef FRAMEWORK_STDPCH_H
#define FRAMEWORK_STDPCH_H

#pragma message ( "--------------------------- GLF STDPCH! -------------------------" )

#include <grfcore/common.h>
#include <grfcore/grfcore.h>
#include <grfcore/shaders/program.h>
#include <grfcore/geometry/geometrybuffer.h>
#include <grfcore/geometry/vertexbuffer.h>
#include <grfcore/geometry/indexbuffer.h>
#include <grfcore/surfaces/texture.h>
#include <grfcore/math/rect.h>
#include <grfcore/math/transform.h>
#include <grfcore/camera/camera.h>

#include <array>

#include <QSet>
#include <QVector>
#include <QList>
#include <QFile>
#include <QDebug>
#include <QMouseEvent>
#include <QColor>
#include <QTimer>

#endif // FRAMEWORK_STDPCH_H
