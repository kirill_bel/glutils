#include "cameraentity.h"
#include "grf/component/inputcomponent.h"

using namespace grf;

CameraEntity::CameraEntity(QObject* parent) :
    Entity(parent)
{

    m_cameraComponent = new CameraComponent(this);
    this->addComponent(m_cameraComponent);
    m_rootComponent = m_cameraComponent;

    m_cameraComponent->fieldOfView = 45;
    m_cameraComponent->aspectRatio = 4.f / 3.f;
    m_cameraComponent->nearPlane = 1.f;
    m_cameraComponent->farPlane = 10000.f;
    m_cameraComponent->updateProjectionMatrix();

    m_inputComponent = new InputComponent(this);
    this->addComponent(m_inputComponent);

    setupInput();
}

void CameraEntity::setupInput()
{
    connect(m_inputComponent, &InputComponent::mouseMoveEvent, this, &CameraEntity::mouseMoveEvent);
    connect(m_inputComponent, &InputComponent::mousePressEvent, this, &CameraEntity::mousePressEvent);
    connect(m_inputComponent, &InputComponent::mouseReleaseEvent, this, &CameraEntity::mouseReleaseEvent);
    connect(m_inputComponent, &InputComponent::mouseWheelEvent, this, &CameraEntity::mouseWheelEvent);
}

void CameraEntity::mouseMoveEvent(const QMouseEvent *event)
{
    glm::vec2 mousePos = grf::toGlmVec(event->pos());

    if((m_mousePrevPos.length() != 0) && (event->buttons() & Qt::LeftButton))
    {
        glm::vec2 mouseDelta = mousePos - m_mousePrevPos;

        m_rootComponent->transform().q = (
                    glm::angleAxis(float(mouseDelta.y) * m_rotateAmount, glm::vec3(1,0,0)) *
                    m_rootComponent->transform().q *
                    glm::angleAxis(-float(mouseDelta.x) * m_rotateAmount, glm::vec3(0,1,0))
                    );

        m_rootComponent->transform().q = glm::normalize(m_rootComponent->transform().q);
        m_rootComponent->updateTransform();
    }
    m_mousePrevPos = mousePos;
}

void CameraEntity::mousePressEvent(const QMouseEvent *event)
{

}

void CameraEntity::mouseReleaseEvent(const QMouseEvent *event)
{

}

void CameraEntity::mouseWheelEvent(const QWheelEvent *event)
{

}

