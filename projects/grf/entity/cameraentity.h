#ifndef CAMERAENTITY_H
#define CAMERAENTITY_H

#include "grf/entity/entity.h"
#include "grf/component/cameracomponent.h"

namespace grf {

class CameraEntity : public Entity
{
    Q_OBJECT
    static constexpr float m_moveAmount = 10;
    static constexpr float m_rotateAmount = 0.005;

public:
    explicit CameraEntity(QObject* parent = nullptr);

protected slots:
    void mouseMoveEvent(const QMouseEvent* event);
    void mousePressEvent(const QMouseEvent* event);
    void mouseReleaseEvent(const QMouseEvent* event);
    void mouseWheelEvent ( const QWheelEvent * event );
protected:
    void setupInput();
private:
    CameraComponent* m_cameraComponent = nullptr;
    glm::vec2 m_mousePrevPos;
};

} // grf

#endif // CAMERAENTITY_H
