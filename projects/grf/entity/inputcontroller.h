#ifndef INPUTCONTROLLER_H
#define INPUTCONTROLLER_H

#include "grf/entity/entity.h"
#include "grf/component/inputcomponent.h"

namespace grf {

class InputController : public Entity
{
    Q_OBJECT
public:
    explicit InputController(QObject* parent = nullptr);

    void broadcastMouseMove(const QMouseEvent* event);
    void broadcastMousePress(const QMouseEvent* event);
    void broadcastMouseRelease(const QMouseEvent* event);
    void broadcastMouseWheel(QWheelEvent * event);

    void pushInputComponent(InputComponent* comp);
    void popInputComponent(InputComponent* comp);

private:
    QList<InputComponent*> m_inputComponents;
};

} // grf

#endif // INPUTCONTROLLER_H
