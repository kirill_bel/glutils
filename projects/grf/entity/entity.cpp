#include "entity.h"
#include "grf/component/inputcomponent.h"
#include "inputcontroller.h"

using namespace grf;

Entity::Entity(QObject *parent) :
    QObject(parent)
{

}

void Entity::addComponent(EntityComponent* comp)
{
    comp->setParent(this);
}

void Entity::removeComponent(EntityComponent* comp)
{
    if(takeComponent(comp))
    {
        delete comp;
    }
}

EntityComponent* Entity::takeComponent(EntityComponent* comp)
{
    if(comp->getOwner() != this)
    {
        assert(false && "Incorrect component owner!");
        return nullptr;
    }

    comp->setParent(nullptr);
    return comp;
}

bool Entity::hasComponent(EntityComponent *comp) const
{
    return m_components.contains(comp);
}

void Entity::enableInput(InputController *ctrl)
{
    if(!ctrl)
        return;

    if(!m_inputComponent)
    {
        m_inputComponent = new InputComponent(this);
//        m_inputComponent->registerComponent();
    }
    else
    {
        ctrl->popInputComponent(m_inputComponent);
    }

    ctrl->pushInputComponent(m_inputComponent);
}

void Entity::disableInput(InputController *ctrl)
{
    if(!ctrl || !m_inputComponent)
        return;


    ctrl->popInputComponent(m_inputComponent);
}

void Entity::modify()
{

}

SceneComponent *Entity::getRootComponent() const
{
    return m_rootComponent;
}

void Entity::childEvent(QChildEvent *event)
{
    QObject *childObject = event->child();
    bool isComponent = childObject->property("EntityComponentType").toBool();
#ifdef DEBUG_CHECKS
    qDebug() << this << "Child event:" << event << isComponent;
#endif

    EntityComponent* entityObject = static_cast<EntityComponent*>(childObject);

    if(isComponent && event->added())
    {
#ifdef DEBUG_CHECKS
        assert(!m_components.contains(entityObject));
#endif
        modify();
        m_components.insert(entityObject);
    }
    else if(event->removed())
    {
        if(m_components.contains(entityObject))
        {
            modify();
            m_components.remove(entityObject);
        }
    }
}
