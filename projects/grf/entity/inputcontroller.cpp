#include "inputcontroller.h"

using namespace grf;

InputController::InputController(QObject *parent) :
    Entity(parent)
{

}

void InputController::broadcastMouseMove(const QMouseEvent *event)
{
    for(InputComponent* comp : m_inputComponents)
        emit comp->mouseMoveEvent(event);

}

void InputController::broadcastMousePress(const QMouseEvent *event)
{
    for(InputComponent* comp : m_inputComponents)
        emit comp->mousePressEvent(event);
}

void InputController::broadcastMouseRelease(const QMouseEvent *event)
{
    for(InputComponent* comp : m_inputComponents)
        emit comp->mouseReleaseEvent(event);
}

void InputController::broadcastMouseWheel(QWheelEvent *event)
{
    for(InputComponent* comp : m_inputComponents)
        emit comp->mouseWheelEvent(event);
}

void InputController::pushInputComponent(grf::InputComponent *comp)
{
    if(!comp)
        return;

    if(!m_inputComponents.contains(comp))
        m_inputComponents.append(comp);
}

void InputController::popInputComponent(InputComponent *comp)
{
    if(!comp)
        return;

    m_inputComponents.removeOne(comp);
}
