#ifndef ENTITY_H
#define ENTITY_H

#include "grf/common.h"

namespace grf {

class EntityComponent;
class InputController;
class InputComponent;
class SceneComponent;
class Entity : public QObject
{
    Q_OBJECT
public:
    explicit Entity(QObject* parent = nullptr);

    void addComponent(EntityComponent* comp);
    void removeComponent(EntityComponent* comp);
    EntityComponent* takeComponent(EntityComponent* comp);

    bool hasComponent(EntityComponent* comp) const;

    void enableInput(InputController* ctrl);
    void disableInput(InputController* ctrl);

    void modify();

    SceneComponent *getRootComponent() const;

    template<typename T>
    QList<T*> getComponents()
    {
        QList<T*> list;
        for(EntityComponent* comp : m_components)
            if(qobject_cast<T*>(comp))
                list.append(static_cast<T*>(comp));
        return list;
    }

private:
    void childEvent(QChildEvent *event) override;
protected:
    QSet<EntityComponent*> m_components;
    InputComponent* m_inputComponent = nullptr;
    SceneComponent* m_rootComponent = nullptr;
};

} // grf

#endif // ENTITY_H
