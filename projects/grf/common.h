#ifndef FRAMEWORK_COMMON_H
#define FRAMEWORK_COMMON_H

#include "stdpch.h"

namespace grf {

template<typename T>
grf::trect<T> toGlRect(const grf::trect<T> &r, const glm::ivec2 &screenSize)
{
    return grf::trect<T>(r.left, screenSize.y - r.top - r.height(),
                        r.width(), r.height());
}

template<typename T>
grf::trect<T> fromGlRect(const grf::trect<T> &r, const glm::ivec2 &screenSize)
{
    return grf::trect<T>(r.left, screenSize.y - r.top - r.height(),
                        r.width(), r.height());
}

template<typename T>
glm::tvec2<T> toGlVec(const glm::tvec2<T> &p, const glm::ivec2 &screenSize)
{
    return glm::tvec2<T>(p.x, screenSize.y - p.y);
}

template<typename T>
glm::tvec2<T> fromGlVec(const glm::tvec2<T> &p, const glm::ivec2 &screenSize)
{
    return glm::tvec2<T>(p.x, screenSize.y - p.y);
}

template<typename T = int>
grf::trect<T> toGrfRect(const QRect& rect)
{
    return grf::trect<T>(rect.left(), rect.top(), rect.width(), rect.height());
}

template<typename T = float>
grf::trect<T> toGrfRect(const QRectF& rect)
{
    return grf::trect<T>(rect.left(), rect.top(), rect.width(), rect.height());
}

template<typename T = int>
glm::tvec2<T> toGlmVec(const QPoint& pt)
{
    return glm::tvec2<T>(pt.x(), pt.y());
}

template<typename T = float>
glm::tvec2<T> toGlmVec(const QPointF& pt)
{
    return glm::tvec2<T>(pt.x(), pt.y());
}

template<typename T = int>
glm::tvec2<T> toGlmVec(const QSize& pt)
{
    return glm::tvec2<T>(pt.width(), pt.height());
}

template<typename T = float>
glm::tvec2<T> toGlmVec(const QSizeF& pt)
{
    return glm::tvec2<T>(pt.width(), pt.height());
}

template<typename T>
QRect toQRect(const grf::trect<T>& rect)
{
    return QRect(rect.left, rect.top, rect.width(), rect.height());
}

template<typename T>
QRectF toQRectF(const grf::trect<T>& rect)
{
    return QRectF(rect.left, rect.top, rect.width(), rect.height());
}

template<typename T>
QPoint toQPoint(const glm::tvec2<T>& pt)
{
    return QPoint(pt.x, pt.y);
}

template<typename T>
QPointF toQPointF(const glm::tvec2<T>& pt)
{
    return QPointF(pt.x, pt.y);
}

template<typename T>
QSize toQSize(const glm::tvec2<T>& pt)
{
    return QSize(pt.x, pt.y);
}

template<typename T>
QSizeF toQSizeF(const glm::tvec2<T>& pt)
{
    return QSizeF(pt.x, pt.y);
}

} // grf

#endif // FRAMEWORK_COMMON_H
