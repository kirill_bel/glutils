#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include "grf/common.h"

namespace grf {

class AssetManager : public QObject
{
    Q_OBJECT
public:
    AssetManager();
};

} // grf

#endif // ASSETMANAGER_H
