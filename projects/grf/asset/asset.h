#ifndef ASSET_H
#define ASSET_H

#include "grf/common.h"

namespace grf {

class Asset
{
public:
    Asset();

    Asset(const QObject *inputAsset);

    QString objectPath; // Path in format: package.group.assetName

    QString packagePath;

    QString groupNames;

    QString assetName;

    QString assetClass;

    QMap<QString, QString> tags;

};

} // grf

#endif // ASSET_H
