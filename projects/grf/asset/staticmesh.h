#ifndef STATICMESH_H
#define STATICMESH_H

#include "grf/common.h"

namespace grf {

enum { MAX_STATIC_TEXCOORDS = 8 };
struct StaticMeshVertex;
struct StaticMeshLOD;

class StaticMesh : public QObject
{
    Q_OBJECT
public:
    explicit StaticMesh(QObject* parent = nullptr);

    QVector<StaticMeshLOD> meshLODs;
};

struct StaticMeshVertex
{
    glm::vec3 pos;

    glm::vec3 tangentX;
    glm::vec3 tangentY;
    glm::vec3 tangentZ;

    glm::vec2 uvs[MAX_STATIC_TEXCOORDS];

    uint32_t color;
};

struct StaticMeshLOD
{
    StaticMeshLOD() :
        vertexBuffer(GL_STATIC_DRAW),
        indexBuffer(IndexBuffer::k32bit)
    {

    }

    VertexBuffer<StaticMeshVertex> vertexBuffer;

    IndexBuffer indexBuffer;
};


} // grf

#endif // STATICMESH_H
