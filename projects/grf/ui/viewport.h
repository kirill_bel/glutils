#ifndef RENDERVIEWPORT_H
#define RENDERVIEWPORT_H

#include "grf/common.h"
#include "grf/entity/inputcontroller.h"
#include "grf/component/cameracomponent.h"

namespace grf {

class Viewport : public QObject
{
    Q_OBJECT
public:
    explicit Viewport(QColor clearColor, QObject* parent = nullptr);

    void render();
    void resize(const QSize& newSize);

    bool isVisible() const;
    void setVisible(bool value);

    QColor clearColor() const;
    void setClearColor(const QColor &clearColor);

    InputController *inputController() const;
    void setInputController(InputController *inputController);

    CameraComponent *cameraComponent() const;
    void setCameraComponent(CameraComponent *cameraComponent);

    virtual void mousePressEvent(const QMouseEvent *event);
    virtual void mouseReleaseEvent(const QMouseEvent *event);
    virtual void mouseMoveEvent(const QMouseEvent *event);

    QSize size() const;

protected:
    void initialize();
    virtual void onInitialization();
    virtual void onRender();
    virtual void onResize(const QSize& newSize);
private:
    bool m_visible = true;
    bool m_initialized = false;
    QColor m_clearColor;
    InputController* m_inputController = nullptr;
    CameraComponent* m_cameraComponent = nullptr;

    grf::GeometryBuffer m_buffer;
    grf::Texture m_texture;
    grf::Program m_shader;
    GLuint m_bufferVAO = 0;
    QSize m_size;
};

} // grf

#endif // RENDERVIEWPORT_H
