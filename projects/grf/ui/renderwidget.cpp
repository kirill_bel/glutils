#include "renderwidget.h"

using namespace grf;

RenderWidget::RenderWidget(QWidget *parent) :
    QOpenGLWidget(parent),
    m_splitterBuffer(sizeof(float) * 2, GL_DYNAMIC_DRAW)
{
    this->setMouseTracking(true);

    m_renderTimer = new QTimer(this);
    m_renderTimer->setInterval(1000 / 60);
    m_renderTimer->setSingleShot(false);
    connect(m_renderTimer, SIGNAL(timeout()), this, SLOT(update()));
}

RenderWidget::~RenderWidget()
{

}

void RenderWidget::initializeGL()
{
    initializeOpenGLFunctions();

    grf::Core::instance().initialize();

    QFile file(":/shaders/default.vert");
    Q_ASSERT(file.open(QIODevice::ReadOnly));
    grf::Shader vertShader(file.readAll().data(), GL_VERTEX_SHADER);
    vertShader.update();

    QFile file1(":/shaders/default.frag");
    Q_ASSERT(file1.open(QIODevice::ReadOnly));
    grf::Shader fragShader(file1.readAll().data(), GL_FRAGMENT_SHADER);
    fragShader.update();

    m_defaultShader.setVertexShader(vertShader);
    m_defaultShader.setFragmentShader(fragShader);
    Q_ASSERT(m_defaultShader.link());

//    glGenVertexArrays(1, &m_splitterVAO);
//    glBindVertexArray(m_splitterVAO);

    m_splitterBuffer.resize(4);
    m_splitterBuffer.bind();

    GLint vertexAttribCoords = m_defaultShader.attributeLocation("qt_Vertex");
    glVertexAttribPointer(vertexAttribCoords, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);
    glEnableVertexAttribArray(vertexAttribCoords);

    m_splitterBuffer.unbind();
//    glBindVertexArray(0);

    m_renderTimer->start();
}

void RenderWidget::paintGL()
{
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_SCISSOR_TEST);
    glEnable(GL_MULTISAMPLE);

    for(auto it = m_viewports.begin(); it != m_viewports.end(); it++)
    {
        if(!it.key()->isVisible())
            continue;

        grf::rect rect = grf::toGlRect(it.value().screenRect, grf::toGlmVec(size()));

        glViewport(rect.left, rect.top, rect.width(), rect.height());
        glScissor(rect.left, rect.top, rect.width(), rect.height());

//        m_defaultShader.setUniform("qt_WorldMatrix", glm::mat4());
//        m_defaultShader.setUniform("qt_ViewProjectionMatrix", glm::mat4());

        it.key()->render();
    }

    glDisable(GL_SCISSOR_TEST);

    glViewport(0,0, width(), height());

    m_defaultShader.bind();

//    glBindVertexArray(m_splitterVAO);

    glDisable(GL_MULTISAMPLE);

    glm::mat4 ortoMat = glm::ortho<float>(0, width(), 0, height());

    m_defaultShader.setUniform("qt_WorldViewProjectionMatrix", ortoMat);
    m_defaultShader.setUniform("qt_AmbientColor", glm::vec4(m_foregroundColor.redF(),
                                                            m_foregroundColor.greenF(),
                                                            m_foregroundColor.blueF(),
                                                            1));

    glLineWidth(1);
    for(ViewportSplitter& splitter : m_splitters)
    {
        grf::rect rect = grf::toGlRect(splitter.screenRect, grf::toGlmVec(size()));
        m_splitterBuffer.at<glm::vec2>(0) = rect.topLeft();
        m_splitterBuffer.at<glm::vec2>(1) = rect.topRight();
        m_splitterBuffer.at<glm::vec2>(2) = rect.bottomLeft();
        m_splitterBuffer.at<glm::vec2>(3) = rect.bottomRight();
        m_splitterBuffer.update();
        m_splitterBuffer.bind();

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        m_splitterBuffer.unbind();
    }

    m_defaultShader.setUniform("qt_AmbientColor", glm::vec4(0,0,0,0));

//    glBindVertexArray(0);
}

void RenderWidget::resizeGL(int width, int height)
{
    moveElement(m_colOffsets, m_colOffsets.size() - 1, width);
    moveElement(m_rowOffsets, m_rowOffsets.size() - 1, height);

    updateScreenRects();    
}

QMouseEvent RenderWidget::convertMouseEvent(QMouseEvent *event, const grf::rect &screenRect)
{
    glm::ivec2 pos = grf::toGlmVec(event->pos());
    glm::ivec2 viewportPos =
        grf::toGlVec(pos - screenRect.topLeft(), screenRect.size());

    return QMouseEvent(event->type(), grf::toQPointF(viewportPos),
                       event->button(),
                       event->buttons(),
                       event->modifiers());
}

QColor RenderWidget::foregroundColor() const
{
    return m_foregroundColor;
}

void RenderWidget::setForegroundColor(const QColor &foregroundColor)
{
    m_foregroundColor = foregroundColor;
}

void RenderWidget::mousePressEvent(QMouseEvent *event)
{
    glm::ivec2 pos = grf::toGlmVec(event->pos());
    for(int i = 0; i < m_splitters.size(); i++)
    {
        if(m_splitters.at(i).screenRect.contains(pos))
        {
            m_dragSplitterIndex = i;
            m_dragOffset = grf::toGlmVec(event->pos()) - m_splitters.at(i).screenRect.center();
            break;
        }
    }

    for(auto it = m_viewports.begin(); it != m_viewports.end(); it++)
        if(it.value().screenRect.contains(pos))
        {
            QMouseEvent convEvent = convertMouseEvent(event, it.value().screenRect);
            it.key()->mousePressEvent(&convEvent);
        }
}

void RenderWidget::mouseReleaseEvent(QMouseEvent *event)
{
    m_dragSplitterIndex = -1;

    glm::ivec2 pos = grf::toGlmVec(event->pos());
    for(auto it = m_viewports.begin(); it != m_viewports.end(); it++)
        if(it.value().screenRect.contains(pos))
        {
            QMouseEvent convEvent = convertMouseEvent(event, it.value().screenRect);
            it.key()->mouseReleaseEvent(&convEvent);
        }
}

void RenderWidget::mouseMoveEvent(QMouseEvent *event)
{
    glm::ivec2 pos = grf::toGlmVec(event->pos());

    if(event->buttons() & Qt::LeftButton)
    {
        if(m_dragSplitterIndex != -1)
            dragSplitter(m_dragSplitterIndex, event->pos());
    }

    bool cursorFound = false;
    for(int i = 0; i < m_splitters.size(); i++)
    {
        if(m_splitters.at(i).screenRect.contains(grf::toGlmVec(event->pos())))
        {
            switch(m_splitters.at(i).orientation)
            {
            case Qt::Horizontal:
                setCursor(QCursor(Qt::SplitHCursor));
                break;
            case Qt::Vertical:
                setCursor(QCursor(Qt::SplitVCursor));
                break;
            }
            cursorFound = true;
            break;
        }
    }
    if(!cursorFound)
    {
        setCursor(QCursor(Qt::ArrowCursor));
    }

    for(auto it = m_viewports.begin(); it != m_viewports.end(); it++)
        if(it.value().screenRect.contains(pos))
        {
            QMouseEvent convEvent = convertMouseEvent(event, it.value().screenRect);
            it.key()->mouseMoveEvent(&convEvent);
        }
}

void RenderWidget::dragSplitter(int splitterIndex, const QPoint &pt)
{
    const ViewportSplitter& splitter = m_splitters.at(splitterIndex);

    switch(splitter.orientation)
    {
    case Qt::Horizontal:
        moveElement(m_colOffsets, splitter.index, pt.x() - m_dragOffset.x);
        break;
    case Qt::Vertical:
        moveElement(m_rowOffsets, splitter.index, pt.y() - m_dragOffset.y);
        break;
    }
    updateScreenRects();
    update();
}

void RenderWidget::moveElement(QVector<float> &arr, int index, float position)
{
    if(arr[index] == position)
        return;

    float oldOffset = arr[index];
    arr[index] = position;

    float size = arr[arr.size() - 1];
    float scaleLeft = (position / oldOffset);
    float scaleRight = ((position - size) / (oldOffset - size));

    for(int i = 1; i < index; i++)
        arr[i] = float(arr[i]) * scaleLeft;

    for(int i = index + 1; i < m_colOffsets.size() - 1; i++)
        arr[i] = size - float(size - arr[i]) * scaleRight;
}

QSize RenderWidget::gridSize() const
{
    return m_gridSize;
}

void RenderWidget::setGridSize(const QSize &gridSize)
{
    m_gridSize = gridSize;

    m_colOffsets.resize(m_gridSize.width() + 1);
    for(int i = 0; i < m_colOffsets.size(); i++)
        m_colOffsets[i] = i * width() / m_colOffsets.size();

    m_rowOffsets.resize(m_gridSize.height() + 1);
    for(int i = 0; i < m_rowOffsets.size(); i++)
        m_rowOffsets[i] = i * height() / m_rowOffsets.size();
}

void RenderWidget::insertViewport(Viewport *viewport, int x, int y, int width, int height)
{
    auto it = m_viewports.find(viewport);
    if(it == m_viewports.end())
        it = m_viewports.insert(viewport, ViewportData());

    it.value().rect = grf::rect(x, y, width, height);
    updateSplitters();
}

void RenderWidget::updateScreenRects()
{
    QList<std::array<int,2>> colOffsetsScreen;
    colOffsetsScreen.reserve(m_colOffsets.size()-1);
    QList<std::array<int,2>> rowOffsetsScreen;
    rowOffsetsScreen.reserve(m_colOffsets.size()-1);

    float halfSplitter = m_viewportSplitterSize / 2.f;

    std::array<int,2> arr;
    for(int i = 0; i < m_colOffsets.size() - 1; i++)
    {
        arr[0] = m_colOffsets.at(i) + (i ? halfSplitter : 0);
        arr[1] = m_colOffsets.at(i+1) - ((i != m_colOffsets.size() - 2) ? halfSplitter : 0);
        colOffsetsScreen.append(arr);
    }

    for(int i = 0; i < m_rowOffsets.size() - 1; i++)
    {
        arr[0] = m_rowOffsets.at(i) + (i ? halfSplitter : 0);
        arr[1] = m_rowOffsets.at(i+1) - ((i != m_rowOffsets.size() - 2) ? halfSplitter : 0);
        rowOffsetsScreen.append(arr);
    }

    for(ViewportData& data : m_viewports)
    {
        data.screenRect =
                grf::rect(
                      glm::ivec2(colOffsetsScreen[data.rect.left][0],
                                 rowOffsetsScreen[data.rect.top][0]),
                      glm::ivec2(colOffsetsScreen[data.rect.right-1][1],
                                 rowOffsetsScreen[data.rect.bottom-1][1]));
    }

    for(ViewportSplitter& splitter : m_splitters)
    {
        switch(splitter.orientation)
        {
        case Qt::Horizontal:
            splitter.screenRect =
                    grf::rect(
                        glm::ivec2(colOffsetsScreen[splitter.index - 1][1],
                                    rowOffsetsScreen[splitter.startIndex][0]),
                        glm::ivec2(colOffsetsScreen[splitter.index][0],
                                    rowOffsetsScreen[splitter.endIndex][1]));
            break;
        case Qt::Vertical:
            splitter.screenRect =
                    grf::rect(
                        glm::ivec2(colOffsetsScreen[splitter.startIndex][0],
                                    rowOffsetsScreen[splitter.index - 1][1]),
                        glm::ivec2(colOffsetsScreen[splitter.endIndex][1],
                                    rowOffsetsScreen[splitter.index][0]));
            break;
        }
    }

    for(auto it = m_viewports.begin(); it != m_viewports.end(); it++)
        it.key()->resize(toQSize(it.value().screenRect.size()));
}

void RenderWidget::updateSplitters()
{
    QVector<QSet<int>> horSplitters;
    horSplitters.resize(m_colOffsets.size());
    QVector<QSet<int>> verSplitters;
    verSplitters.resize(m_rowOffsets.size());

    for(ViewportData& data : m_viewports)
    {
        for(int i = data.rect.left; i < data.rect.right; i++)
        {
            verSplitters[data.rect.top].insert(i);
            verSplitters[data.rect.bottom].insert(i);
        }

        for(int i = data.rect.top; i < data.rect.bottom; i++)
        {
            horSplitters[data.rect.left].insert(i);
            horSplitters[data.rect.right].insert(i);
        }
    }

    m_splitters.clear();
    for(int splitterIdx = 1; splitterIdx < verSplitters.size() - 1; splitterIdx++)
    {
        QList<int> values = verSplitters.at(splitterIdx).toList();
        qSort(values);

        int startValue = 0;
        int prevValue = 0;
        for(int i = 0; i < values.size(); i++)
        {
            int value = values.at(i);
            if((value > prevValue + 1) || (i == values.size() - 1))
            {
                ViewportSplitter splitter;
                splitter.index = splitterIdx;
                splitter.startIndex = startValue;
                splitter.endIndex = value;
                splitter.orientation = Qt::Vertical;
                m_splitters.append(splitter);
                startValue = value;
            }
            prevValue = value;
        }
    }

    for(int splitterIdx = 1; splitterIdx < horSplitters.size() - 1; splitterIdx++)
    {
        QList<int> values = horSplitters.at(splitterIdx).toList();
        qSort(values);

        int startValue = 0;
        int prevValue = 0;
        for(int i = 0; i < values.size(); i++)
        {
            int value = values.at(i);
            if((value > prevValue + 1) || (i == values.size() - 1))
            {
                ViewportSplitter splitter;
                splitter.index = splitterIdx;
                splitter.startIndex = startValue;
                splitter.endIndex = value;
                splitter.orientation = Qt::Horizontal;
                m_splitters.append(splitter);
                startValue = value;
            }
            prevValue = value;
        }
    }
}
