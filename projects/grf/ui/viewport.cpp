#include "viewport.h"
#include <QImage>

using namespace grf;

Viewport::Viewport(QColor clearColor, QObject* parent) :
    QObject(parent),
    m_clearColor(clearColor),
    m_buffer(0)
{

}

void Viewport::render()
{
    if(!m_initialized)
        initialize();

    onRender();
}

void Viewport::resize(const QSize &newSize)
{
    m_size = newSize;
    onResize(newSize);
}

bool Viewport::isVisible() const
{
    return m_visible;
}

void Viewport::setVisible(bool value)
{
    m_visible = value;
}

QColor Viewport::clearColor() const
{
    return m_clearColor;
}

void Viewport::setClearColor(const QColor &clearColor)
{
    m_clearColor = clearColor;
}

InputController *Viewport::inputController() const
{
    return m_inputController;
}

void Viewport::setInputController(InputController *inputController)
{
    m_inputController = inputController;
}

CameraComponent *Viewport::cameraComponent() const
{
    return m_cameraComponent;
}

void Viewport::setCameraComponent(CameraComponent *cameraComponent)
{
    m_cameraComponent = cameraComponent;
}

void Viewport::mousePressEvent(const QMouseEvent *event)
{
    if(m_inputController)
        m_inputController->broadcastMousePress(event);
}

void Viewport::mouseReleaseEvent(const QMouseEvent *event)
{
    if(m_inputController)
        m_inputController->broadcastMouseRelease(event);
}

void Viewport::mouseMoveEvent(const QMouseEvent *event)
{
    if(m_inputController)
        m_inputController->broadcastMouseMove(event);
}

void Viewport::initialize()
{
    assert(!m_initialized);
    m_initialized = true;
    onInitialization();
}

void Viewport::onInitialization()
{
    struct Vertex
    {
        glm::vec3 pos;
        glm::vec2 tc;
        int color = 0;
    };

    QFile file(":/shaders/testdraw.vert");
    Q_ASSERT(file.open(QIODevice::ReadOnly));
    grf::Shader vertShader(file.readAll().data(), GL_VERTEX_SHADER);
    vertShader.update();

    QFile file1(":/shaders/testdraw.frag");
    Q_ASSERT(file1.open(QIODevice::ReadOnly));
    grf::Shader fragShader(file1.readAll().data(), GL_FRAGMENT_SHADER);
    fragShader.update();

    m_shader.setVertexShader(vertShader);
    m_shader.setFragmentShader(fragShader);
    Q_ASSERT(m_shader.link());


    QImage image = QImage("F:/Programming/Cpp/OpenCVContourTest/flower512.png").convertToFormat(QImage::Format_RGBA8888).mirrored();
//    QImage image(512, 512, QImage::Format_RGBA8888);
//    image.fill(Qt::white);

    m_texture.setData(image.scanLine(0), image.width(), image.height());
    m_texture.update();

    m_buffer = grf::GeometryBuffer(sizeof(Vertex), GL_STATIC_DRAW);
    m_buffer.resize(3);
//    m_buffer.at<Vertex>(0).pos = glm::vec3(-10, 0, -15);
//    m_buffer.at<Vertex>(0).color = qRgba(255,0,0,255);
//    m_buffer.at<Vertex>(0).tc = glm::vec2(0,0);
//    m_buffer.at<Vertex>(1).pos = glm::vec3(-10, 5, -10);
//    m_buffer.at<Vertex>(1).color = qRgba(0,255,0,255);
//    m_buffer.at<Vertex>(1).tc = glm::vec2(0.5,1);
//    m_buffer.at<Vertex>(2).pos = glm::vec3(-10, 0, -5);
//    m_buffer.at<Vertex>(2).color = qRgba(0,0,255,255);
//    m_buffer.at<Vertex>(2).tc = glm::vec2(1,0);
    m_buffer.at<Vertex>(0).pos = glm::vec3(-5, 0, 0);
    m_buffer.at<Vertex>(0).color = qRgba(255,0,0,255);
    m_buffer.at<Vertex>(0).tc = glm::vec2(0,0);

    m_buffer.at<Vertex>(1).pos = glm::vec3(0, 5, 0);
    m_buffer.at<Vertex>(1).color = qRgba(0,255,0,255);
    m_buffer.at<Vertex>(1).tc = glm::vec2(0.5,1);

    m_buffer.at<Vertex>(2).pos = glm::vec3(5, 0, 0);
    m_buffer.at<Vertex>(2).color = qRgba(0,0,255,255);
    m_buffer.at<Vertex>(2).tc = glm::vec2(1,0);
    m_buffer.update();
    m_buffer.bind();

    m_shader.bind();

    GLint i1 = glGetAttribLocation(m_shader.id(), "qt_Vertex");
    GLint i2 = glGetAttribLocation(m_shader.id(), "qt_Texcoord");
    GLint i3 = glGetAttribLocation(m_shader.id(), "qt_VertexColor");

    GLint vertexAttribCoords = m_shader.attributeLocation("qt_Vertex");
    glVertexAttribPointer(vertexAttribCoords, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
    glEnableVertexAttribArray(vertexAttribCoords);
    grf::checkOpenGLError(false);

    GLint vertexAttribTexcoord = m_shader.attributeLocation("qt_Texcoord");
    grf::checkOpenGLError(false);
    glVertexAttribPointer(vertexAttribTexcoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), BUFFER_OFFSET(12));
    grf::checkOpenGLError(false);
    glEnableVertexAttribArray(vertexAttribTexcoord);
    grf::checkOpenGLError(false);

    GLint vertexAttribColors = m_shader.attributeLocation("qt_VertexColor");
    glVertexAttribPointer(vertexAttribColors, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), BUFFER_OFFSET(20));
    glEnableVertexAttribArray(vertexAttribColors);
    grf::checkOpenGLError(false);

    m_buffer.unbind();
}

void Viewport::onRender()
{
    glClearColor(m_clearColor.redF(),m_clearColor.greenF(),m_clearColor.blueF(),1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    grf::Camera c;
    c.setFovAngle(glm::radians(35.f));
    c.setAspect(1.7777);
    c.update();

    m_shader.bind();

    if(m_cameraComponent == nullptr)
    {
        glm::mat4 mvp = c.projection() *
                glm::lookAt(glm::vec3(0,0,-10), glm::vec3(0,0,-9), glm::vec3(0,1,0));
        m_shader.setUniform("qt_WorldViewProjectionMatrix", mvp);
    }
    else
    {
        m_cameraComponent->aspectRatio = float(size().width()) / float(size().height());
        m_cameraComponent->updateProjectionMatrix();
        m_shader.setUniform("qt_WorldViewProjectionMatrix", m_cameraComponent->projectionMatrix() * m_cameraComponent->viewMatrix());
    }

    glUniform1i(m_shader.uniformIdx("qt_Texture0"), 0);
//    m_shader.setUniform("qt_Texture0", 0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texture.id());


    m_buffer.bind();

    glDrawArrays(GL_TRIANGLES, 0, 3);

    m_buffer.unbind();

//    m_shader.unbind();

}

void Viewport::onResize(const QSize &newSize)
{

}

QSize Viewport::size() const
{
    return m_size;
}

