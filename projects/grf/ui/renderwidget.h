#ifndef RENDERWIDGET_H
#define RENDERWIDGET_H

#include "grf/common.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include "grf/ui/viewport.h"

namespace grf {

class RenderWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
    struct ViewportData
    {
        ViewportData() {}
        grf::rect screenRect;
        grf::rect rect;
    };
    struct ViewportSplitter
    {
        Qt::Orientation orientation = Qt::Horizontal;
        grf::rect screenRect;
        int index = 0;
        int startIndex = 0;
        int endIndex = 0;
    };
public:
    RenderWidget(QWidget *parent = 0);
    ~RenderWidget();

    QSize gridSize() const;
    void setGridSize(const QSize &gridSize);

    void insertViewport(Viewport* viewport, int x, int y, int width = 1, int height = 1);

    QColor foregroundColor() const;
    void setForegroundColor(const QColor &foregroundColor);

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    void updateScreenRects();
    void updateSplitters();
    void dragSplitter(int splitterIndex, const QPoint& pt);
    void moveElement(QVector<float>& arr, int index, float position);

    QMouseEvent convertMouseEvent(QMouseEvent* event, const grf::rect& screenRect);
private:
    QSize m_gridSize;
    QColor m_foregroundColor = QColor(Qt::white);

    QVector<float> m_colOffsets;
    QVector<float> m_rowOffsets;

    QMap<Viewport*, ViewportData> m_viewports;
    float m_viewportSplitterSize = 6;
    float m_viewportMinimumSize = 25;
    QList<ViewportSplitter> m_splitters;
    grf::Program m_defaultShader;
    GLuint m_splitterVAO = 0;
    grf::GeometryBuffer m_splitterBuffer;

    glm::ivec2 m_dragOffset;
    int m_dragSplitterIndex = -1;

    QTimer* m_renderTimer = nullptr;
};

} // grf


#endif // RENDERWIDGET_H
