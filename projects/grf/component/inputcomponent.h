#ifndef INPUTCOMPONENT_H
#define INPUTCOMPONENT_H

#include "entitycomponent.h"

namespace grf {

class InputComponent : public EntityComponent
{
    Q_OBJECT
public:
    explicit InputComponent(Entity* parent = nullptr);

signals:
    void mouseMoveEvent(const QMouseEvent* event);
    void mousePressEvent(const QMouseEvent* event);
    void mouseReleaseEvent(const QMouseEvent* event);
    void mouseWheelEvent ( const QWheelEvent * event );
};

} // grf

#endif // INPUTCOMPONENT_H
