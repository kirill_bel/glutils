#ifndef CAMERACOMPONENT_H
#define CAMERACOMPONENT_H

#include "scenecomponent.h"

namespace grf {

class CameraComponent : public SceneComponent
{
    Q_OBJECT
public:
    enum ProjectionType
    {
        kOrthographic,
        kPerspective
    };
public:
    explicit CameraComponent(Entity* parent = nullptr);

    float fieldOfView = 90.f;

    float nearPlane = 0.1f;

    float farPlane = 10000.f;

    float aspectRatio = 1.77777f;

    float orthoWidth = 1.f;

    float orthoHeight = 1.f;

    ProjectionType projectionType = kPerspective;

    const glm::dmat4& viewMatrix() const;

    const glm::dmat4&  projectionMatrix() const;

    void updateProjectionMatrix();

protected:
    void onTransformUpdated() override;
private:
    glm::dmat4 m_viewMatrix;
    glm::dmat4 m_projectionMatrix;
};

} // grf

#endif // CAMERACOMPONENT_H
