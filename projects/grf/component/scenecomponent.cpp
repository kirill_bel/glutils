#include "scenecomponent.h"

using namespace grf;

SceneComponent::SceneComponent(Entity *parent) :
    EntityComponent(parent)
{

}

SceneComponent::~SceneComponent()
{
    if(m_attachParent)
        detachFromComponent(m_attachParent);

    for(SceneComponent* child : m_attachChildren)
        if(child)
            child->detachFromComponent(this);
}

bool SceneComponent::attachToComponent(SceneComponent *parent)
{
    if((parent == nullptr) || (parent == this))
        return false;

    if(parent == m_attachParent)
        return true;

    if(m_attachParent)
        detachFromComponent(m_attachParent);

    if(parent->isAttachedTo(this))
        return false;

    m_attachParent = parent;
    parent->m_attachChildren.append(this);
    parent->onChildAttached(this);
    return true;
}

void SceneComponent::detachFromComponent(SceneComponent *parent)
{
    if(m_attachParent == nullptr)
        return;

    if(!m_attachParent->m_attachChildren.contains(this))
    {
        assert(false && "Detach is wrong!");
        return;
    }

    m_attachParent->m_attachChildren.removeOne(this);
    m_attachParent->onChildAttached(this);
    m_attachParent = nullptr;
}

//void SceneComponent::setupAttachment(SceneComponent *parent)
//{
//    assert(isRegistered());
//    assert(parent == nullptr || !parent->m_attachChildren.contains(this));

//    m_attachParent = parent;
//}

const QList<SceneComponent *> &SceneComponent::getAttachedChildren() const
{
    return m_attachChildren;
}

bool SceneComponent::isAttachedTo(SceneComponent *other) const
{
    return m_attachParent == other;
}

bool SceneComponent::isComponentAttached(SceneComponent *child)
{
    return child->isAttachedTo(this);
}

SceneComponent *SceneComponent::getAttachParent() const
{
    return m_attachParent;
}


Transform &SceneComponent::transform()
{
    return m_transformToParent;
}

const Transform &SceneComponent::worldTransform() const
{
    return m_transformToWorld;
}

void SceneComponent::updateTransform()
{
    if(m_attachParent && !m_attachParent->m_worldToComponentUpdated)
    {
        m_attachParent->updateTransform();

        if(m_worldToComponentUpdated)
            return;
    }

    m_worldToComponentUpdated = true;

    grf::Transform newTransform;
    newTransform = calcWorldTransform(m_transformToParent);

    bool bHasChanged = !m_transformToWorld.equals(newTransform, 1.e-8f);
    if(bHasChanged)
    {
        m_transformToWorld = newTransform;
        propagateTransformUpdate(true);
    }
    else
    {
        propagateTransformUpdate(false);
    }
}

grf::Transform SceneComponent::calcWorldTransform(const grf::Transform &newRelativeTransform)
{
    if(m_attachParent)
    {
        return newRelativeTransform * m_attachParent->worldTransform();
    }

    return newRelativeTransform;
}

void SceneComponent::propagateTransformUpdate(bool transformChanged)
{
    if(transformChanged)
    {
        onTransformUpdated();
    }

    updateChildTransforms();
}

void SceneComponent::updateChildTransforms()
{
    for(SceneComponent* comp : m_attachChildren)
    {
        comp->updateTransform();
    }
}

void SceneComponent::updateBounds()
{

}
