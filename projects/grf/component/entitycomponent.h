#ifndef ENTIRYCOMPONENT_H
#define ENTIRYCOMPONENT_H

#include "grf/common.h"

namespace grf {

class Entity;
class EntityComponent : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool EntityComponentType MEMBER m_entityComponentType CONSTANT FINAL)
public:
    explicit EntityComponent(Entity* parent = nullptr);

    bool isRegistered() const;
//    void registerComponent();
//    void unregisterComponent();

    Entity* getOwner() const;

protected:
    virtual void onComponentCreated();
    virtual void onComponentDestroyed();
    virtual void onRegister();
    virtual void onUnregister();
private:
    bool m_componentCreated = false;
    bool m_componentRegistered = false;
    const bool m_entityComponentType = true;
};

} // grf

#endif // ENTIRYCOMPONENT_H
