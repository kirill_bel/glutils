#ifndef PRIMITIVECOMPONENT_H
#define PRIMITIVECOMPONENT_H

#include "scenecomponent.h"

namespace grf {

class PrimitiveComponent : public SceneComponent
{
    Q_OBJECT
public:
    explicit PrimitiveComponent(Entity* parent = nullptr);

    bool selectable = true;

    virtual int materialCount() const {return 0;}
    virtual class Material* materialAt(int idx) const {return nullptr;}
    virtual void setMaterial(int idx, class Material* material) {}

signals:
    void inputMouseClicked();
    void inputMouseReleased();
    void inputMouseOver();
};

} // grf

#endif // PRIMITIVECOMPONENT_H
