#include "cameracomponent.h"

using namespace grf;

CameraComponent::CameraComponent(Entity *parent) :
    SceneComponent(parent)
{

}

void CameraComponent::onTransformUpdated()
{
    glm::mat4 rotate = glm::mat4_cast(worldTransform().q);
    glm::mat4 translate = glm::mat4(1.0f);
    translate = glm::translate(translate, -worldTransform().p);
    m_viewMatrix = rotate * translate;
}

const glm::dmat4 &CameraComponent::projectionMatrix() const
{
    return m_projectionMatrix;
}

void CameraComponent::updateProjectionMatrix()
{
    switch(projectionType)
    {
    case kOrthographic:
        m_projectionMatrix = glm::ortho(0.f, 0.f,
                                        orthoWidth,
                                        orthoHeight,
                                        nearPlane,
                                        farPlane);
        break;
    case kPerspective:
        m_projectionMatrix = glm::perspective(glm::radians(fieldOfView),
                                              aspectRatio,
                                              nearPlane,
                                              farPlane);
        break;
    }
}

const glm::dmat4 &CameraComponent::viewMatrix() const
{
    return m_viewMatrix;
}
