#ifndef SCENECOMPONENT_H
#define SCENECOMPONENT_H

#include "entitycomponent.h"

namespace grf {

class SceneComponent : public EntityComponent
{
    Q_OBJECT
public:
    explicit SceneComponent(Entity* parent = nullptr);
    ~SceneComponent();

    bool attachToComponent(SceneComponent* parent);
    void detachFromComponent(SceneComponent* parent);
    const QList<SceneComponent *>& getAttachedChildren() const;

    bool isAttachedTo(SceneComponent* other) const;
    bool isComponentAttached(SceneComponent* child);

    SceneComponent *getAttachParent() const;

    Transform &transform();
    const Transform& worldTransform() const;

    void updateTransform();

//    void setupAttachment(SceneComponent* parent);

protected:
    virtual void onChildAttached(SceneComponent* comp) {}
    virtual void onChildDetached(SceneComponent* comp) {}
    virtual void onTransformUpdated() {}
protected:
    inline grf::Transform calcWorldTransform(const grf::Transform& newRelativeTransform);
    void propagateTransformUpdate(bool transformChanged);
    void updateChildTransforms();
    void updateBounds();
private:
    grf::Transform m_transformToWorld;
    grf::Transform m_transformToParent;
    SceneComponent* m_attachParent = nullptr;
    QList<SceneComponent*> m_attachChildren;

    bool m_worldToComponentUpdated = false;
};

} // grf

#endif // SCENECOMPONENT_H
