#include "entitycomponent.h"
#include "grf/entity/entity.h"

using namespace grf;

EntityComponent::EntityComponent(Entity *parent)
{
    this->setParent(parent);
}

bool EntityComponent::isRegistered() const
{
    return m_componentRegistered;
}

//void EntityComponent::registerComponent()
//{
//    if(isRegistered())
//    {
//        // TODO: mesage
//        return;
//    }

//    Entity* owner = getOwner();

////    assert(!owner || owner->hasComponent(this));

//    if(!m_componentCreated)
//    {
//        onComponentCreated();
//    }

//    onRegister();
//}

//void EntityComponent::unregisterComponent()
//{
//    if(!isRegistered())
//    {
//        // TODO: mesage
//        return;
//    }

//}

Entity *EntityComponent::getOwner() const
{
    return qobject_cast<Entity*>(parent());
}

void EntityComponent::onComponentCreated()
{
    assert(!m_componentCreated);
    m_componentCreated = true;
}

void EntityComponent::onComponentDestroyed()
{
    assert(m_componentCreated);
    m_componentCreated = false;
}

void EntityComponent::onRegister()
{
    assert(!m_componentRegistered);
    m_componentRegistered = true;
}

void EntityComponent::onUnregister()
{
    assert(m_componentRegistered);
    m_componentRegistered = false;
}
