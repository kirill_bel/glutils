#ifndef STATICMESHCOMPONENT_H
#define STATICMESHCOMPONENT_H

#include "primitivecomponent.h"

namespace grf {

class StaticMeshComponent : public PrimitiveComponent
{
    Q_OBJECT
public:
    explicit StaticMeshComponent(Entity *parent = nullptr);

};

} // grf

#endif // STATICMESHCOMPONENT_H
