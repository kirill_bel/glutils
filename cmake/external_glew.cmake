set(GLEW_PATH_HINT "${DEPENDENCIES_INSTALL_PATH}" CACHE INTERNAL ""  FORCE)

unset(GLEW_LIBRARY_CHECK CACHE)
find_library(GLEW_LIBRARY_CHECK
     NAMES glew glewd glew32 glew32d GLEW GLEWd
     HINTS ${GLEW_PATH_HINT}/glew/lib
     PATHS ${GLEW_PATH_HINT}/glew/lib
     NO_DEFAULT_PATH
     NO_CMAKE_PATH
     NO_CMAKE_ENVIRONMENT_PATH
     NO_CMAKE_SYSTEM_PATH
)

if(NOT FORCE_REBUILD_THIRD_PARTY AND GLEW_LIBRARY_CHECK)
    message("GLEW library found at ${GLEW_LIBRARY_CHECK}")
else()
    message("GLEW not found! Building GLEW library...")

    externalproject_add(glew_library
    PREFIX glew_library
    SOURCE_DIR ${DEPENDENCIES_SOURCE_PATH}/glew/build/cmake
    EXCLUDE_FROM_ALL TRUE
    CMAKE_ARGS
        -DCMAKE_MAKE_PROGRAM=${CMAKE_MAKE_PROGRAM}
        -DCMAKE_INSTALL_PREFIX=${DEPENDENCIES_INSTALL_PATH}/glew
        -DCMAKE_BUILD_TYPE=${DEPENDENCIES_BUILD_TYPE}
        -DBUILD_UTILS=OFF
        -DGLEW_OSMESA=ON
    )

    list(APPEND TEMP_EXTERNAL_DEPENDENCIES glew_library)
endif()
