set(GLM_PATH_HINT "${DEPENDENCIES_INSTALL_PATH}" CACHE INTERNAL ""  FORCE)

unset(GLM_LIBRARY_CHECK CACHE)
find_path(GLM_LIBRARY_CHECK
    NAMES glm.hpp
    HINTS ${GLEW_PATH_HINT}/glm/include/glm
    PATHS ${GLEW_PATH_HINT}/glm/include/glm
    NO_DEFAULT_PATH
    NO_CMAKE_PATH
    NO_CMAKE_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    )

if(NOT FORCE_REBUILD_THIRD_PARTY AND GLM_LIBRARY_CHECK)
    message("GLM library found at ${GLM_LIBRARY_CHECK}")
else()
    message("GLM not found! Building GLM library...")

    externalproject_add(glm_library
        PREFIX glm_library
        SOURCE_DIR ${DEPENDENCIES_SOURCE_PATH}/glm
        EXCLUDE_FROM_ALL TRUE
        CMAKE_ARGS
            -DCMAKE_MAKE_PROGRAM=${CMAKE_MAKE_PROGRAM}
            -DCMAKE_INSTALL_PREFIX=${DEPENDENCIES_INSTALL_PATH}/glm
            -DCMAKE_BUILD_TYPE=${DEPENDENCIES_BUILD_TYPE}
        )

    list(APPEND TEMP_EXTERNAL_DEPENDENCIES glm_library)
endif()
